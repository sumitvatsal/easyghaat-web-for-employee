//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 600) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 900, 'easeInOutExpo');
        event.preventDefault();
    });
});

// scroll header change logo

$(window).scroll(function() {
    if ($(window).scrollTop() >= 470) {
        $(".logo").addClass("scrolling");
    } else {
        $(".logo").removeClass("scrolling");
    }
	
	
});

/*$(window).scroll(function(){       
   var scroll_start = 0;
   var startchange = $('.navbar-right');
   var offset = startchange.offset();
   $(window).scroll(function() { 
      scroll_start = $(this).scrollTop();
      if(scroll_start > offset.top) {
          $('.navbar-right>li>a').css('color','#FFFFFF');
       } else {
          $('.navbar-right>li>a').css('color','#000000');
       }
   });
});*/

$(document).ready(function(){       
            var scroll_pos = 0;
            $(document).scroll(function() { 
                scroll_pos = $(this).scrollTop();
                if(scroll_pos >= 470) {
                    $("nav").css('background-color', '#003264');
                    $("nav a").css('color', 'white');
					
                } else {
                    $("nav").css('background-color', 'white');
                    $("nav a").css('color', '#003264');
					
                }
            });
			
        });

