/*
jQuery(function($) {
  "use strict";
 $.noConflict();
     $('.nav a').on('click', function(){ 
        if($('.navbar-toggle').css('display') !='none'){
            $(".navbar-toggle").trigger( "click" );
        }
    });
*/
//  Navigation scrolling

/* tab script */


 $('#interest_tabs').on('click', 'a[data-toggle="tab"]', function(e) {
      e.preventDefault();

      var $link = $(this);

      if (!$link.parent().hasClass('active')) {

        //remove active class from other tab-panes
        $('.tab-content:not(.' + $link.attr('href').replace('#','') + ') .tab-pane').removeClass('active');

        // click first submenu tab for active section
        $('a[href="' + $link.attr('href') + '_all"][data-toggle="tab"]').click();

        // activate tab-pane for active section
        $('.tab-content.' + $link.attr('href').replace('#','') + ' .tab-pane:first').addClass('active');
      }

    });

/*left contant */

$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});

/* end left containt */
/* end tab script */
/*password */
$(".reveal").on('click',function() {
    var $pwd = $(".pwd");
    if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
    } else {
        $pwd.attr('type', 'password');
    }
});

/*end password */





      $('a.page-scroll').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('slider,body').animate({
                scrollTop: target.offset().top - 600
              }, 900);
              return false;
            }
          }
        });

   


 
  // accordian
  $('.accordion-toggle').on('click', function(){
    $(this).closest('.panel-group').children().each(function(){
    $(this).find('>.panel-heading').removeClass('active');
     });

    $(this).closest('.panel-heading').toggleClass('active');
  });

 
 /* ----------------------------------------------------------- */
/*  BX slider
/* ----------------------------------------------------------- */

      //Portfolio item and blog slider
    
/*Smooth Scroll*/
     smoothScroll.init({
          speed: 400,
          easing: 'easeInQuad',
          offset:0,
          updateURL: true,
          callbackBefore: function ( toggle, anchor ) {},
          callbackAfter: function ( toggle, anchor ) {}
        }); 


  /* ----------------------------------------------------------- */
  /*  Main slideshow
  /* ----------------------------------------------------------- */

  $('#slider_part').carousel({
    pause: true,
    interval: 100000,
  });

  /* ----------------------------------------------------------- */
 /*ISotope Portfolio
 /* ----------------------------------------------------------- */   
    
      var $container = $('.portfolio-wrap');
      var $filter = $('#isotope-filter');
      // Initialize isotope 
      $container.isotope({
          filter: '*',
          layoutMode: 'fitRows',
          animationOptions: {
              duration: 750,
              easing: 'linear'
          }
      });
      // Filter items when filter link is clicked
      $filter.find('a').click(function () {
          var selector = $(this).attr('data-filter');
          $filter.find('a').removeClass('current');
          $(this).addClass('current');
          $container.isotope({
              filter: selector,
              animationOptions: {
                  animationDuration: 750,
                  easing: 'linear',
                  queue: false,
              }
          });
          return false;
      }); 


  // Portfolio Isotope
    
    
    var container = $('.portfolio-wrap'); 
    
      function splitColumns() { 
        var winWidth = $(window).width(), 
          columnNumb = 1;
        
        
        if (winWidth > 1024) {
          columnNumb = 4;
        } else if (winWidth > 900) {
          columnNumb = 2;
        } else if (winWidth > 479) {
          columnNumb = 2;
        } else if (winWidth < 479) {
          columnNumb = 1;
        }
        
        return columnNumb;
      }   
      
      function setColumns() { 
        var winWidth = $(window).width(), 
          columnNumb = splitColumns(), 
          postWidth = Math.floor(winWidth / columnNumb);
        
        container.find('.portfolio-box').each(function () { 
          $(this).css( { 
            width : postWidth + 'px' 
          });
        });
      }   
      
      function setProjects() { 
        setColumns();
        container.isotope('reLayout');
      }   
      
      container.imagesLoaded(function () { 
        setColumns();
      });
      
    
      $(window).bind('resize', function () { 
        setProjects();      
      });

   
   
   
   

  /* ----------------------------------------------------------- */
  /* Team Carousel
  /* ----------------------------------------------------------- */

  $("#owl-demo").owlCarousel({
  
  navigation : true, // Show next and prev buttons
  // navigationText: ["prev","next"], 
   navigationText: [
      "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>"
      ],
  slideSpeed : 300,
  paginationSpeed : 400,
  autoPlay: true,  
  items : 4,
  itemsDesktop:[1199,4],  
  itemsDesktopSmall:[979,3],  //As above.
  itemsTablet:[768,3],    //As above.
  // itemsTablet:[640,2],   
  itemsMobile:[479,1],    //As above
  goToFirst: true,    //Slide to first item if autoPlay reach end
  goToFirstSpeed:1000 
  });




    //Testimonial

    $("#testimonial-carousel").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 600,
      pagination:false,
      singleItem:true
 
    });

    // Custom Navigation Events
    var owl = $("#testimonial-carousel");


    // Custom Navigation Events
    $(".next").click(function(){
      owl.trigger('owl.next');
    })
    $(".prev").click(function(){
      owl.trigger('owl.prev');
    })
    $(".play").click(function(){
      owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
    })
    $(".stop").click(function(){
      owl.trigger('owl.stop');
    })
    

//Clients
//
    $("#client-carousel").owlCarousel({

      navigation : true, // Show next and prev buttons
      navigationText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      slideSpeed : 400,
      pagination:false,
      items : 5,
      rewindNav: true,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
      itemsTablet:[768,3],    //As above.
      // itemsMobile:[479,2],
      itemsMobile:[320,1],
      stopOnHover:true,
      autoPlay:true

    });


  /* ----------------------------------------------------------- */
  /* Team Carousel
  /* ----------------------------------------------------------- */

  $("#owl-blog").owlCarousel({
    autoPlay: true,  
    items : 4,
    itemsDesktop:[1199,4], 
    itemsDesktopSmall:[979,3],  //As above.
    itemsTablet:[768,2],    //As above.
    itemsMobile:[479,1],    //As above
    goToFirst: true,    //Slide to first item if autoPlay reach end
    goToFirstSpeed:1000, 
  
  });


//Counter 
  
   
    // jQuery(document).ready(function( $ ) {
        $('.counter').counterUp({
            delay: 100,
            time: 2000
        });
    // }); 

// prettyphoto

 $("a[data-rel^='prettyPhoto']").prettyPhoto();
 
    
 //nav bar
 


/* ==============================================
Back To Top Button
=============================================== */  
 /*
  $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
      // scroll body to 0px on click
      $('#back-top').click(function () {
          $('#back-top a').tooltip('hide');
          $('body,html').animate({
              scrollTop: 0
          }, 700);
          return false;
      });
      
      $('#back-top').tooltip('hide');




}); */

/*=============================================
 Back to top
=============================================*/

$(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

});
/*==================================
Sub menu
===================================*/

$(document).ready(function(){
  $('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });
});

/* feed back rating */
// Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function($, window) {
  var Starrr;

  Starrr = (function() {
    Starrr.prototype.defaults = {
      rating: void 0,
      numStars: 5,
      change: function(e, value) {}
    };

    function Starrr($el, options) {
      var i, _, _ref,
        _this = this;

      this.options = $.extend({}, this.defaults, options);
      this.$el = $el;
      _ref = this.defaults;
      for (i in _ref) {
        _ = _ref[i];
        if (this.$el.data(i) != null) {
          this.options[i] = this.$el.data(i);
        }
      }
      this.createStars();
      this.syncRating();
      this.$el.on('mouseover.starrr', 'span', function(e) {
        return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('mouseout.starrr', function() {
        return _this.syncRating();
      });
      this.$el.on('click.starrr', 'span', function(e) {
        return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
      });
      this.$el.on('starrr:change', this.options.change);
    }

    Starrr.prototype.createStars = function() {
      var _i, _ref, _results;

      _results = [];
      for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
        _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
      }
      return _results;
    };

    Starrr.prototype.setRating = function(rating) {
      if (this.options.rating === rating) {
        rating = void 0;
      }
      this.options.rating = rating;
      this.syncRating();
      return this.$el.trigger('starrr:change', rating);
    };

    Starrr.prototype.syncRating = function(rating) {
      var i, _i, _j, _ref;

      rating || (rating = this.options.rating);
      if (rating) {
        for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }
      }
      if (rating && rating < 5) {
        for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
          this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
      }
      if (!rating) {
        return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
      }
    };

    return Starrr;

  })();
  return $.fn.extend({
    starrr: function() {
      var args, option;

      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      return this.each(function() {
        var data;

        data = $(this).data('star-rating');
        if (!data) {
          $(this).data('star-rating', (data = new Starrr($(this), option)));
        }
        if (typeof option === 'string') {
          return data[option].apply(data, args);
        }
      });
    }
  });
})(window.jQuery, window);

$(function() {
  return $(".starrr").starrr();
});

$( document ).ready(function() {
      
  $('#stars').on('starrr:change', function(e, value){
    $('#count').html(value);
  });
  
  $('#stars-existing').on('starrr:change', function(e, value){
    $('#count-existing').html(value);
  });
});

