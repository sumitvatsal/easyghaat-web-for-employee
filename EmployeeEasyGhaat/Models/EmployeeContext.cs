﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EmployeeEasyGhaat.Models
{
    public class EmployeeContext:DbContext
    {
        //Integrated with DB for Employee Dashboard 
        public DbSet<Order_LaundryBag_Tbl> Order_LaundryBag_Tbls { get; set; }
        public DbSet<LaundryBag_Tbl> LaundryBag_Tbls { get; set; }
        public DbSet<Laundry_Bag> Laundry_Bags { get; set; }
        public DbSet<OrderDetail_tbl> OrderDetail_tbls { get; set; }
        public DbSet<Make_My_Package> Make_My_Packages { get; set; }
        public DbSet<Package_Transctn_tbl> Package_Transctn_tbls { get; set; }
        public DbSet<MinmumOrder_Value_Tbl> MinmumOrder_Value_Tbls { get; set; }
        public DbSet<DeliveryType_SurgePrice> DeliveryType_SurgePrices { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Promo_Code_Service> Promo_Code_Services { get; set; }
        public DbSet<OrderDetail_tbl_view> OrderDetail_tbl_views { get; set; }
        public DbSet<PromoCode_Service> PromoCode_Services { get; set; }
        public DbSet<Tax_tbl> Tax_tbls { get; set; }
        public DbSet<Transaction_tbl> Transaction_tbls { get; set; }
        public DbSet<Wallet_tbl> Wallet_tbls { get; set; }
        public DbSet<CustomerInvoice_Tbl> CustomerInvoice_Tbls { get; set; }
        public DbSet<TaskAssignment_tbl> TaskAssignment_tbls { get; set; }
        public DbSet<Cus_Invoice_Wallet_Order> Cus_Invoice_Wallet_Orders { get; set; }
        public DbSet<PickUp_tbl> PickUp_tbls { get; set; }
        public DbSet<EmpCashReceiving_Tbl> EmpCashReceiving_Tbls { get; set; }
        public DbSet<Cashback_tbl> Cashback_tbls { get; set; }
        public DbSet<ReferalAmount_Tbl> ReferalAmount_Tbls { get; set; }
        public DbSet<Customer_Register> Customer_Registers { get; set; }
        public DbSet<TaskAssign_Incent_view> TaskAssign_Incent_views { get; set; }
        public DbSet<EmployeeIncentivePetrol_tbl> EmployeeIncentivePetrol_tbls { get; set; }
        public DbSet<TimeSlot> TimeSlots { get; set; }
        public DbSet<EmpIncentivePetrolDetail_Tbl> EmpIncentivePetrolDetail_Tbls { get; set; }
        public DbSet<OrderDelivery_tbl> OrderDelivery_tbls { get; set; }
        public DbSet<VendorHandOver_tbl> VendorHandOver_tbls { get; set; }
        public DbSet<CashHandOver_Tbl> CashHandOver_Tbls { get; set; } 
        public DbSet<EmployeeAttendence> EmployeeAttendences { get; set; }

        //NITIN CONTEXT
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Branch> Branchs { get; set; }
        public DbSet<PriceTable> PriceTables { get; set; }
        public DbSet<Service> Sercvices { get; set; }
        public DbSet<VendorService> VendorServices { get; set; }
        public DbSet<Get_OrderInfo> Get_OrderInfos { get; set; }
        public DbSet<Get_OrderDetailInfo> Get_OrderDetailInfos { get; set; }
        public DbSet<Get_DeliveryTime> Get_DeliveryTimes { get; set; }
        public DbSet<Get_PickUpTime> Get_PickUpTimes { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<OrderCancel> OrderCancels { get; set; }
        public DbSet<Order_tbl> Order_tbls { get; set; }
        public DbSet<Get_PaymentType> Get_PaymentTypes { get; set; }
        public DbSet<Get_SurgeAmount> Get_SurgeAmounts { get; set; }
        public DbSet<Get_PickUpDeliveryDate> Get_PickUpDeliveryDates { get; set; }
        public DbSet<Get_PickUpAddress> Get_PickUpAddresses { get; set; }
        public DbSet<Get_DeliveryAddress> Get_DeliveryAddresses { get; set; }
        public DbSet<RoleType> RoleTypes { get; set; }
        public DbSet<EmployeeSession> EmployeeSessions { get; set; }
        public DbSet<Emp_Info> Emp_Infos { get; set; }
        public DbSet<EmployeeReaction_tbl> EmployeeReaction_tbls { get; set; }
        public DbSet<EmployeeRejactionReason> EmployeeRejactionReasons { get; set; }
        public DbSet<CancelReason> CancelReasons { get; set; }


      

    }
}