﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    [Table("TimeSlot")]
    public class TimeSlot
    {
        public long id { get; set; }
        public int BranchId { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public int TaskLimit { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}