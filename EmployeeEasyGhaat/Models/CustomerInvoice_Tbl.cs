﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    [Table("CustomerInvoice_Tbl")]
    public class CustomerInvoice_Tbl
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public System.DateTime InvoiceDateTime { get; set; }
        public double SubAmount { get; set; }
        public Nullable<double> PickUp_DeliveryCharges { get; set; }
        public Nullable<double> SurgingAmount { get; set; }
        public double PackageAdjustmentAmt { get; set; }
        public Nullable<double> LaundryBagAmount { get; set; }
        public Nullable<double> CashBackAmount { get; set; }
        public double ServiceTaxAmt { get; set; }
        public double SwachhBharatCess { get; set; }
        public double KrishiKalyanCessAmt { get; set; }
        public double TotalAmount { get; set; }
        public double RoundOffAmount { get; set; }
        public Nullable<double> WalletAdjustment { get; set; }
        public double NetBalance { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

     
    }
}




