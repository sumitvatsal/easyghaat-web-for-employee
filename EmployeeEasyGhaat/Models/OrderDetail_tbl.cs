﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    [Table("OrderDetail_tbl")]
    public class OrderDetail_tbl
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int PriceId { get; set; }
        public int Qunatity { get; set; }
        public double SubAmount { get; set; }
        public Nullable<int> Length { get; set; }
        public Nullable<int> Breadth { get; set; }
        public int CustomerId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}