﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;

namespace EmployeeEasyGhaat.Models
{
    //Note : Date Pass In Format MM/dd/yyyy 12/9/16 
    public class Connection
    {
        public static string connstring = ConfigurationManager.ConnectionStrings["EmployeeContext"].ConnectionString;
        static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
        public static DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
    }

    [Table("Order_LaundryBag_Tbl")]
    public class Order_LaundryBag_Tbl
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "OrderId cannot be blank")]
        public int OrderId { get; set; }
        [Required(ErrorMessage = "LaundryBagId cannot be blank")]
        public int LaundryBagId { get; set; }
        public int Count { get; set; }
        public int SubAmount { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }

    }
    [Table("LaundryBag_Tbl")]
    public class LaundryBag_Tbl
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "OrderId cannot be blank")]
        public string MaterialName { get; set; }
        [Required(ErrorMessage = "LaundryBagId cannot be blank")]

        public string BagImgUrl { get; set; }
        [Required]
        public int weightCapacity { get; set; }
        [Required]
        public int CountCapacity { get; set; }
        [Required]
        public int Price { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }

    [Table("Laundry_Bag")]
    public class Laundry_Bag
    {
        [Key]
        public int Id { get; set; }
        public string MaterialName { get; set; }
        public string BagImgUrl { get; set; }
        public int weightCapacity { get; set; }
        public int CountCapacity { get; set; }
        public int Price { get; set; }
    }

    [Table("OrderDetail_tbl")]
    public class OrderDetail_tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int OrderId { get; set; }
        [Required]
        public int PriceId { get; set; }
        [Required]
        public int Qunatity { get; set; }
        public double SubAmount { get; set; }
        public int? Length { get; set; }
        public int? Breadth { get; set; }
        [Required]
        public int CustomerId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
    }

    [Table("OrderDetail_tbl_view")]
    public class OrderDetail_tbl_view
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int PriceId { get; set; }
        public string Product_Name { get; set; }
        public double Price { get; set; }
        public int Qunatity { get; set; }
        public double SubAmount { get; set; }
        public int? Length { get; set; }
        public int? Breadth { get; set; }
        public int CustomerId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public int BranchId { get; set; }
        public int CategoryId { get; set; }
        public string Cat_Name { get; set; }
        public int ServiceId { get; set; }
        public string Ser_Name { get; set; }
        public int UnitId { get; set; }
        public string U_Type { get; set; }
        public int GenderId { get; set; }
        public string Gen_Name { get; set; }


    }

    [Table("PromoCode_Service")]
    public class PromoCode_Service
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int PromoCode_Id { get; set; }
        [Required]
        public int ServiceId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
       
    }
    [Table("MinmumOrder_Value_Tbl")]
    public class MinmumOrder_Value_Tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int BranchId { get; set; }
        [Required]
        public int MinimumOrderAmount { get; set; }       
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }

    [Table("DeliveryType_SurgePrice")]
    public class DeliveryType_SurgePrice
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int DeliveryTypeId { get; set; }
        [Required]
        public int BranchId { get; set; }
        [Required]
        public int SurgePercentage { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }        
    }
    [Table("Make_My_Package")]
    public class Make_My_Package
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int CustomerId { get; set; }
        [Required]
        public int BranchId { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int ServiceId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime To_Date { get; set; }
        public DateTime Purchase_Datetime { get; set; }
        [Required]
        public int Pack_Count { get; set; }
        public double Price { get; set; }
        [Required]
        public int Pay_StatusId { get; set; }
        public double Balance { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
    }

    [Table("Package_Transctn_tbl")]
    public class Package_Transctn_tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int OrderId { get; set; }
        [Required]
        public int PackageID { get; set; }
        [Required]
        public int OrderDetailID { get; set; }
        [Required]
        public int AdjustedCount { get; set; }
        [Required]
        public int ClosingBal { get; set; }
        [Required]
        public double AmountAdjustment { get; set; }      
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
    }

    [Table("Tax_tbl")]
    public class Tax_tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public string TaxName { get; set; }
        [Required]
        public double TaxPercentage { get; set; }
        [Required]
        public string Description { get; set; }           
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
    }

    [Table("PromoCode")]
    public class PromoCode
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Promo Code cannot be blank")]
        public string Promo_Code { get; set; }
        [Required]
        public int MaxUsers { get; set; }
        [Required(ErrorMessage = "ExpiryDate cannot be blank")]
        public DateTime ExpiryDate { get; set; }
        [Required]
        public double MinOrder { get; set; }
        [Required]
        public int PromoType { get; set; }
        public int? Percentage { get; set; }
        public int? MaxDiscount { get; set; }
        public int UsedCount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public double? FlatAmount { get; set; }
        [Required]
        public int AppliedId { get; set; }



    }
    [Table("Promo_Code_Service")]
    public class Promo_Code_Service
    {
        [Key]
        public int Id { get; set; }
        public string Promo_Code { get; set; }
        public int MaxUsers { get; set; }
        public DateTime ExpiryDate { get; set; }
        public double MinOrder { get; set; }
        public int PromoType { get; set; }
        public int? Percentage { get; set; }
        public int? MaxDiscount { get; set; }
        public int UsedCount { get; set; }
        public int AppliedId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public double? FlatAmount { get; set; }
        public int PromoCode_Id { get; set; }
        public int ServiceId { get; set; }

    }

    [Table("EmpCashReceiving_Tbl")]
    public class EmpCashReceiving_Tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int TaskId { get; set; }
        [Required]
        public double TotalAmount { get; set; }
        [Required]
        public bool IsHandOver { get; set; }
        public int? HandoverId { get; set; }//
        public DateTime? HandOverDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        
    }
    [Table("Cashback_tbl")]
    public class Cashback_tbl
    {
        [Key]
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public string Cashback_type { get; set; }
        public int? CustomerId { get; set; }
        public int? RcrgId { get; set; }
        public double Amount { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? Trans_Date { get; set; }
        public DateTime? CreateDate { get; set; }

    }
    [Table("ReferalAmount_Tbl")]
    public class ReferalAmount_Tbl
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Refer_Roleid cannot be blank")]
        public int Refer_Roleid { get; set; }
        [Required(ErrorMessage = "Amount cannot be blank")]
        public int Amount { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }


    [Table("Customer_Register")]
    public class Customer_Register
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public Int64 Mob_no { get; set; }
        public string Pwd { get; set; }
        public string MyReferalCode { get; set; }
        public string FrndReferalCode { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
    }

    [Table("TaskAssign_Incent_view")]
    public class TaskAssign_Incent_view
    {
        [Key]
        public int TaskID { get; set; }
        public int OrderId { get; set; }
        public int EmpId { get; set; }
        public int TaskTypeId { get; set; }
        public int BranchId { get; set; }
        public int RoleTypeId { get; set; }     
    }

    [Table("EmployeeIncentivePetrol_tbl")]
    public class EmployeeIncentivePetrol_tbl
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int BranchId { get; set; }
        [Required]
        public int RoleTypeId { get; set; }
        public double IncentiveAmount { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        [Required]
        public double PetrolAmount { get; set; }
    }

    [Table("EmpIncentivePetrolDetail_Tbl")]
    public class EmpIncentivePetrolDetail_Tbl
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int TaskId { get; set; }
        [Required]
        public double IncentiveAmount { get; set; }
        [Required]
        public double PetrolAmount { get; set; }
        [Required]
        public bool IsTransferred { get; set; }
        public int? BankTransferId { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        [Required]
        public DateTime CreateDate { get; set; }     
    }

    [Table("TimeSlot")]
    public class TimeSlot
    {
        [Key]
        public Int64 id { get; set; }
        public int BranchId { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public int TaskLimit { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }

    }
    [Table("VendorHandOver_tbl")]
    public class VendorHandOver_tbl
    {
        [Key]
        public int Id { get; set; }
        public int PickUpId { get; set; }
        public DateTime HandOverDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ExpectedTakeOverTime { get; set; }        

    }
    [Table("CashHandOver_Tbl")]
    public class CashHandOver_Tbl
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int FromEmpId { get; set; }
        [Required]
        public int ToEmployeeId { get; set; }
        [Required]
        public double TotalAmount { get; set; }
        [Required]
        public DateTime HandOverDate { get; set; }
        [Required]
        public bool IsSubmitted { get; set; }        
        public int? SubmitId { get; set; }
        public bool IsActive { get; set; }       
        public bool IsDeleted { get; set; }       
        public DateTime CreateDate { get; set; }      
        public DateTime? ModifyDate { get; set; }

    }

    [Table("EmployeeAttendence")]
    public class EmployeeAttendence
    {
        [Key]
        public Int64 Id { get; set; }       
        public DateTime Date { get; set; }        
        public DateTime InTime { get; set; }
        public DateTime? OutTime { get; set; }      
        public double? TotalActiveHours { get; set; }        
        public string Remarks { get; set; }
        public string Status { get; set; }
        [Required]
        public int EmpId { get; set; }        
        public bool IsActive { get; set; }        
        public bool IsDeleted { get; set; }
       

    }


    //Class Not linking with Tables
    public class Cashhandover
    {
        public string PendingIds { get; set; }
        public int FromEmpId { get; set; }
        public int ToEmpId { get; set; }        
    }

    public class EmployeeEstatement
    {
        public int EmployeeId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }


    //NITIN MODELS
    [Table("Vendor")]
    public class Vendor
    {
        [Key]
        public int Id { get; set; }
        public string VendorName { get; set; }
        public string Password { get; set; }
        public string Per_Address { get; set; }
        public Nullable<int> CityId { get; set; }
        public string PhoneNo { get; set; }
        public string CurrentAddress { get; set; }
        public Nullable<int> AssignBranchId { get; set; }
        public string UploadPhoto { get; set; }
        public string Note { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> Per_CityId { get; set; }
        public Nullable<int> Per_StateId { get; set; }
        public string Per_PhoneNo { get; set; }
        public Nullable<int> CurrentStateId { get; set; }
        public Nullable<System.DateTime> DOJ { get; set; }
        public Nullable<System.DateTime> DateOfContract { get; set; }
        public string ServiceTaxNo { get; set; }

    }


    [Table("Branch")]
    public class Branch
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNo { get; set; }
        public string EmailId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }




    [Table("PriceTable")]
    public class PriceTable
    {

        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int BranchId { get; set; }
        public double Price { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> ServiceId { get; set; }
        public Nullable<int> UnitId { get; set; }
    }


    [Table("Service")]
    public class Service
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

    }



    [Table("VendorService")]

    public class VendorService
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string VendorName { get; set; }
        public int ServiceId { get; set; }
        public int VendorId { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }



    [Table("Get_OrderInfo")]
    public class Get_OrderInfo
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<Int64> PickUpTimeSlotId { get; set; }
        public Nullable<Int64> DeliveryTimeSlotId { get; set; }
        public Nullable<int> DeliveryTypeId { get; set; }
        public Nullable<int> PaymentTypeId { get; set; }
        public Nullable<int> OrderTypeId { get; set; }
        public int PickUpAddressId { get; set; }
        public int DeliveryAddressId { get; set; }
        public Nullable<int> CustomerId { get; set; }
    }


    [Table("Get_OrderDetailInfo")]
    public class Get_OrderDetailInfo
    {
        [Key]
        public int Id { get; set; }
        public Nullable<System.DateTime> PickUpDate { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public int Qunatity { get; set; }
        public double SubAmount { get; set; }
        public int Length { get; set; }
        public int Breadth { get; set; }
        public double Price { get; set; }
        public string Name { get; set; }
    }


    [Table("Get_PickUpAddress")]
    public class Get_PickUpAddress
    {
        [Key]
        public int Id { get; set; }
        public int PickUpAddressId { get; set; }
        public string CityName { get; set; }
        public string HosueNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }

    }


    [Table("Get_DeliveryAddress")]
    public class Get_DeliveryAddress
    {

        [Key]
        public int Id { get; set; }
        public int DeliveryAddressId { get; set; }
        public string CityName { get; set; }
        public string HosueNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }

    }

    [Table("Get_PickUpTime")]
    public class Get_PickUpTime
    {
        [Key]
        public int OrderId { get; set; }
        public Int64 PickUpTimeSlotId { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public Int64 id { get; set; }
    }


    [Table("Get_DeliveryTime")]
    public class Get_DeliveryTime
    {
        [Key]
        public int OrderId { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public Int64 id { get; set; }
        public Int64 DeliveryTimeSlotId { get; set; }
    }


    [Table("Get_SurgeAmount")]
    public class Get_SurgeAmount
    {
        [Key]
        public int Id { get; set; }
        public int BranchId { get; set; }
        public string SurgeAmount { get; set; }

    }


    [Table("Get_PaymentType")]
    public class Get_PaymentType
    {
        [Key]
        public int Id { get; set; }
        public string PaymentType { get; set; }

    }


    [Table("Get_PickUpDeliveryDate")]
    public class Get_PickUpDeliveryDate
    {
        [Key]
        public int Id { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public Nullable<System.DateTime> PickUpDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public Nullable<System.DateTime> DeliveryDate { get; set; }

    }

    [Table("Employee")]
    public class Employee
    {
        [Key]
        public int emp_id { get; set; }
        [Required]
        public string emp_name { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public int assin_roleId { get; set; }
        public int? asin_branchId { get; set; }
        public string uplod_photo { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public bool Isdeleted { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public string per_phoneNo { get; set; }        
        public DateTime? DOB { get; set; }
        [Required]
        public DateTime DOJ { get; set; }
        [Required]
        public string EmailId { get; set; }
        [Required]
        public int SecretPin { get; set; }

    }


    [Table("RoleType")]
    public class RoleType
    {
        [Key]
        public int Id { get; set; }
        public string RoleName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }



    [Table("EmployeeSession")]
    public class EmployeeSession
    {
        [Key]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string LoginFrom { get; set; }
        public Nullable<DateTime> Login_DateTime { get; set; }
        public Nullable<DateTime> LogOut_DateTime { get; set; }
    }



    [Table("OrderCancel")]
    public class OrderCancel
    {
        [Key]
        public int Id { get; set; }
        public int CancelById { get; set; }
        public int ReasonId { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public int OrderId { get; set; }
        public int RoleTypeId { get; set; }
    }


    [Table("Order_tbl")]
    public class Order_tbl
    {
        public int Id { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<long> PickUpTimeSlotId { get; set; }
        public Nullable<System.DateTime> PickUpDate { get; set; }
        public Nullable<long> DeliveryTimeSlotId { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<int> DeliveryTypeId { get; set; }
        public Nullable<int> ExpectedCount { get; set; }
        public Nullable<int> PaymentTypeId { get; set; }      
        public Nullable<int> OrderTypeId { get; set; }    
        public Nullable<int> PaymentStatusId { get; set; }
        public Nullable<int> OrderStatusId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string PromoCode { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public int PickUpAddressId { get; set; }
        public int DeliveryAddressId { get; set; }
        public Nullable<bool> UsedPkgBalance { get; set; }
        public Nullable<bool> UsedEsyWallet { get; set; }
        public Nullable<System.DateTime> Modified_date { get; set; }
    }

    [Table("Transaction_tbl")]
    public class Transaction_tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int WalletId { get; set; }
        public string Trans_Type { get; set; }
        public double Amount { get; set; }
        public double Closing_Bal { get; set; }
        public int? CashbckId { get; set; }
        public int? RcrhgId { get; set; }
        public int? OrderId { get; set; }
        public DateTime? Trans_Date { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }

    }


    [Table("Wallet_tbl")]
    public class Wallet_tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int CustomerId { get; set; }
        public double Net_Amount { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? Modify_Date { get; set; }

    }

   
    [Table("TaskAssignment_tbl")]
    public class TaskAssignment_tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int OrderId { get; set; }
        public int EmpId { get; set; }
        public int? VendorId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }
        public int TaskTypeId { get; set; }
        public DateTime AssignmentDateTime { get; set; }
        public int AssignedById { get; set; }

    }

    [Table("Cus_Invoice_Wallet_Order")]
    public class Cus_Invoice_Wallet_Order
    {
        [Key]
        public int InvoiceId { get; set; }        
        public int OrderId { get; set; }
        public double WalletAdjustment { get; set; }
        public double NetBalance { get; set; }//
        public double CashBackAmount { get; set; }
        public int CustomerId { get; set; }
        public int? PaymentTypeId { get; set; }
        public int WalletId { get; set; }
        public double Wallet_Amount { get; set; }    

    }

    [Table("PickUp_tbl")]
    public class PickUp_tbl
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int TaskId { get; set; }
        [Required]
        public int EmpId { get; set; }
        [Required]
        public DateTime PickUpDate { get; set; }
        public byte?[] CustomerSign { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreateDate { get; set; }

    }

    [Table("OrderDelivery_tbl")]
    public class OrderDelivery_tbl
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int TaskId { get; set; }
        public byte?[] CustomerSign { get; set; }
        [Required]      
        public DateTime DeliveryDate { get; set; }    
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }

    }
    [Table("CustomerInvoice_Tbl")]
    public class CustomerInvoice_Tbl
    {
        [Key]
        public int Id { get; set; }
        // Foreign Key
        [Required]
        public int OrderId { get; set; }
        [Required]
        public DateTime InvoiceDateTime { get; set; }
        [Required]
        public double SubAmount { get; set; }
        public double PickUp_DeliveryCharges { get; set; }
        public double SurgingAmount { get; set; }
        public double PackageAdjustmentAmt { get; set; }
        public double LaundryBagAmount { get; set; }
        public double CashBackAmount { get; set; }
        public double ServiceTaxAmt { get; set; }
        public double SwachhBharatCess { get; set; }
        public double KrishiKalyanCessAmt { get; set; }
        public double TotalAmount { get; set; }
        public double RoundOffAmount { get; set; }
        public double WalletAdjustment { get; set; }
        public double NetBalance { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }       
        public DateTime? ModifiedDate { get; set; }

    }

  
    [Table("Emp_Info")]
    public class Emp_Info
    {
        [Key]
        public int emp_id { get; set; }        
        public int asin_branchId { get; set; }
        public int RoleId { get; set; }
        public string emp_name { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<System.DateTime> DOJ { get; set; }
        public string EmailId { get; set; }
        public string Name { get; set; }
        public string per_phoneNo { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> Isdeleted { get; set; }
        public string RoleName { get; set; }
        public int SecretPin { get; set; }

    }

    [Table("CancelReason")]
    public class CancelReason
    {
        [Key]
        public int Id { get; set; }
        public string Reason { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> RoleId { get; set; }

    }

    [Table("EmployeeRejactionReason")]
    public class EmployeeRejactionReason
    {
        [Key]
        public int Id { get; set; }
        public string Reason { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> Isdeleted { get; set; }
        public Nullable<DateTime> CreateDateTime { get; set; }

    }
    

    [Table("EmployeeReaction_tbl")]
    public class EmployeeReaction_tbl
    {
        [Key]
        public int Id { get; set; }
        public int TaskId { get; set; }
        public bool EmpAccepted { get; set; }
        public Nullable<System.DateTime> ReactionDateTime { get; set; }
        public Nullable<int> RejectionReasonId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<bool> IsReassigned { get; set; }
    }



  
}