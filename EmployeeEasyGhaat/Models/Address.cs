﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    [Table("Address")]
    public class Address
    {
        public int Id { get; set; }
        public int AddTypeId { get; set; }
        public int CityId { get; set; }
        public string AreaId { get; set; }
        public string HosueNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public int CustomerId { get; set; }

      
    }
}