﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace TestProjects.Models
{
    public class EasyGhaatDbContext : DbContext
    {
        public EasyGhaatDbContext()
            : base("EasyghaatDBCon")
        { }
      
        public DbSet<Address> Address { get; set; }
        public DbSet<CustomerInvoice_Tbl> CustomerInvoice_Tbl { get; set; }
        public DbSet<Customer_Register> Customer_Register { get; set; }
        public DbSet<DeliveryType_tbl> DeliveryType_tbl { get; set; }
        public DbSet<Order_tbl> Order_tbl { get; set; }
        public DbSet<OrderDetail_tbl> OrderDetail_tbl { get; set; }
        public DbSet<TimeSlot> TimeSlot { get; set; }
        public DbSet<PriceTable> PriceTable { get; set; }
        public DbSet<Service> Service { get; set; }
        public DbSet<City> City { get; set; }
    }
}