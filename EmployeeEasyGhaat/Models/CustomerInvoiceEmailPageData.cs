﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    [Table("CustomerInvoice_Tbl")]
    public class CustomerInvoiceEmailPageData
    {
        public Address PickUpAddress { get; set; }
        public Address DeliveryAddress { get; set; }
        public CustomerInvoice_Tbl CustomerInvoice_Tbl { get; set; }
        public Customer_Register Customer_Register { get; set; }
        public DeliveryType_tbl DeliveryType_tbl { get; set; }
        public Order_tbl Order_tbl { get; set; }
        public IEnumerable<OrderDetail_tbl> OrderDetail_tbl { get; set; }
        public TimeSlot PickUpTimeSlot { get; set; }
        public TimeSlot DeliveryTimeSlot { get; set; }
        public IEnumerable<Service> Service { get; set; }
        public IEnumerable<PriceTable> PriceTable { get; set; }
        public City PickUpCity { get; set; }
        public City DeliveryCity { get; set; }
        public IEnumerable<MyClothesClass> MyClothesClass { get; set; }
    }
}