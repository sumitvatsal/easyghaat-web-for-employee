﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    [Table("Order_tbl")]
    public class Order_tbl
    {
        public int Id { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<long> PickUpTimeSlotId { get; set; }
        public Nullable<System.DateTime> PickUpDate { get; set; }
        public Nullable<long> DeliveryTimeSlotId { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<int> DeliveryTypeId { get; set; }
        public Nullable<int> ExpectedCount { get; set; }
        public Nullable<int> PaymentTypeId { get; set; }
        public int OrderTypeId { get; set; }
        public int PaymentStatusId { get; set; }
        public int OrderStatusId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string PromoCode { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public int PickUpAddressId { get; set; }
        public int DeliveryAddressId { get; set; }
        public bool UsedPkgBalance { get; set; }
        public Nullable<bool> UsedEsyWallet { get; set; }
        public Nullable<System.DateTime> Modified_date { get; set; }
    }
}