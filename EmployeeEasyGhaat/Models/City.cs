﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    [Table("City")]
    public class City
    {
        public int Id { get; set; }
        public string CityName { get; set; }
        public bool IsActive { get; set; }
        public int StateId { get; set; }
    }
}