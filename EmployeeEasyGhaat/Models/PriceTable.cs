﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    public class PriceTable
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int BranchId { get; set; }
        public double Price { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> ServiceId { get; set; }
        public Nullable<int> UnitId { get; set; }
    }
}