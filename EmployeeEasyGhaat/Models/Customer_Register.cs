﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestProjects.Models
{
    [Table("Customer_Register")]
    public class Customer_Register
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Int64 Mob_No { get; set; }
        public string EmailId { get; set; }
        public string Pwd { get; set; }
        public string MyReferalCode { get; set; }
        public string FrndReferalCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
    }

}