﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;

namespace EmployeeEasyGhaat.Controllers
{
    public class Order_LaundryBag_TblController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // GET: api/Order_LaundryBag_Tbl
        public IQueryable<Order_LaundryBag_Tbl> GetOrder_LaundryBag_Tbls()
        {
            return db.Order_LaundryBag_Tbls;
        }

        // GET: api/Order_LaundryBag_Tbl/5
        [ResponseType(typeof(Order_LaundryBag_Tbl))]
        public async Task<IHttpActionResult> GetOrder_LaundryBag_Tbl(int id)
        {
            Order_LaundryBag_Tbl order_LaundryBag_Tbl = await db.Order_LaundryBag_Tbls.FindAsync(id);
            if (order_LaundryBag_Tbl == null)
            {
                return NotFound();
            }

            return Ok(order_LaundryBag_Tbl);
        }

        // PUT: api/Order_LaundryBag_Tbl/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrder_LaundryBag_Tbl(int id, Order_LaundryBag_Tbl order_LaundryBag_Tbl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order_LaundryBag_Tbl.Id)
            {
                return BadRequest();
            }
            
            db.Entry(order_LaundryBag_Tbl).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Order_LaundryBag_TblExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Order_LaundryBag_Tbl
        [ResponseType(typeof(Order_LaundryBag_Tbl))]
        public async Task<IHttpActionResult> PostOrder_LaundryBag_Tbl(Order_LaundryBag_Tbl order_LaundryBag_Tbl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (order_LaundryBag_Tbl.Count <= 0)
            {
                return BadRequest("Count cannot be zero");
            }
            else
            {
                //Get Laundry Details
                var Lundry_dtl = db.LaundryBag_Tbls.SingleOrDefault(ly => ly.Id == order_LaundryBag_Tbl.LaundryBagId);
                if (Lundry_dtl != null)
                {

                    order_LaundryBag_Tbl.SubAmount = Lundry_dtl.Price * order_LaundryBag_Tbl.Count;
                    order_LaundryBag_Tbl.IsActive = true;
                    order_LaundryBag_Tbl.IsDeleted = false;
                    order_LaundryBag_Tbl.CreateDate = Connection.indianTime;
                    db.Order_LaundryBag_Tbls.Add(order_LaundryBag_Tbl);
                    await db.SaveChangesAsync();

                    return CreatedAtRoute("DefaultApi", new { id = order_LaundryBag_Tbl.Id }, order_LaundryBag_Tbl);
                }
                else
                {
                    return BadRequest("LaundryId does not Exist.");
                }
            }

           
        }

        [Route("api/Order_LaundryBag_Tbl/UpdateLaundry/{Id}/{count}")]
        [HttpGet]
        public async Task<IHttpActionResult> UpdateLaundry(int Id, int count)
        { 
            //Response Received through status Code:
            //200 => OK(Update Successfully)          
            //404 => Not Found(Entry No exist)

            Order_LaundryBag_Tbl order_LaundryBag_Tbl = await db.Order_LaundryBag_Tbls.FindAsync(Id);
            if(order_LaundryBag_Tbl==null)
            {
                return BadRequest("404");
            }
            order_LaundryBag_Tbl.SubAmount = (order_LaundryBag_Tbl.SubAmount / order_LaundryBag_Tbl.Count) * count;
            order_LaundryBag_Tbl.Count = count;
            db.Entry(order_LaundryBag_Tbl).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return Ok(200);
        }


        [Route("api/Order_LaundryBag_Tbl/GetOrderLaundryBag/ByOrderId/{OrderId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetOrderLaundryBag(int OrderId)
        {
            //Response Received through status Code:
            //Data => OK(Received)          
            //404 => Not Found(Entry No exist)
            //400 => Error(Error Occured)
            try
            {
                var Ord_Ldry = await (from OL in db.Order_LaundryBag_Tbls
                                      join LB in db.LaundryBag_Tbls
                                      on OL.LaundryBagId equals LB.Id
                                      where OL.OrderId == OrderId
                                      select new
                                      {
                                          OrderBagId=OL.Id,
                                          LB.MaterialName,
                                          OL.Count,
                                          OL.SubAmount
                                      }).ToListAsync();

                if (Ord_Ldry == null || Ord_Ldry.Count == 0)
                {
                    return BadRequest("404");
                }
                return Ok(Ord_Ldry);
            }
            catch(Exception ex)
            {
                return BadRequest("400");
            }
           
        }


        // DELETE: api/Order_LaundryBag_Tbl/5      
        public async Task<IHttpActionResult> DeleteOrder_LaundryBag_Tbl(int id)
        {
            //Response Received through status Code:
            //200 => OK(Deleted)          
            //404 => Not Found(Entry No exist)         
            Order_LaundryBag_Tbl order_LaundryBag_Tbl = await db.Order_LaundryBag_Tbls.FindAsync(id);
            if (order_LaundryBag_Tbl == null)
            {
                return BadRequest("404");
            }

            db.Order_LaundryBag_Tbls.Remove(order_LaundryBag_Tbl);
            await db.SaveChangesAsync();

            return Ok(200);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Order_LaundryBag_TblExists(int id)
        {
            return db.Order_LaundryBag_Tbls.Count(e => e.Id == id) > 0;
        }
    }
}