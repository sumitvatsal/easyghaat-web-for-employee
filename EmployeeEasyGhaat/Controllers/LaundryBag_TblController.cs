﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;

namespace EmployeeEasyGhaat.Controllers
{
    public class LaundryBag_TblController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // GET: api/LaundryBag_Tbl
        public IQueryable<LaundryBag_Tbl> GetLaundryBag_Tbls()
        {
            return db.LaundryBag_Tbls;
        }

        [Route("api/LaundryBag_Tbl/Details")]
        public IQueryable<Laundry_Bag> GetAllLaundryBag()
        {
            return db.Laundry_Bags;
        }

        // GET: api/LaundryBag_Tbl/5
        [ResponseType(typeof(LaundryBag_Tbl))]
        public async Task<IHttpActionResult> GetLaundryBag_Tbl(int id)
        {
            LaundryBag_Tbl laundryBag_Tbl = await db.LaundryBag_Tbls.FindAsync(id);
            if (laundryBag_Tbl == null)
            {
                return NotFound();
            }

            return Ok(laundryBag_Tbl);
        }

        // PUT: api/LaundryBag_Tbl/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLaundryBag_Tbl(int id, LaundryBag_Tbl laundryBag_Tbl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != laundryBag_Tbl.Id)
            {
                return BadRequest();
            }

            db.Entry(laundryBag_Tbl).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LaundryBag_TblExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LaundryBag_Tbl
        [ResponseType(typeof(LaundryBag_Tbl))]
        public async Task<IHttpActionResult> PostLaundryBag_Tbl(LaundryBag_Tbl laundryBag_Tbl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            laundryBag_Tbl.IsActive = true;
            laundryBag_Tbl.IsDeleted = false;
            laundryBag_Tbl.CreateDate =  Connection.indianTime;
            laundryBag_Tbl.ModifiedDate =  Connection.indianTime;
            db.LaundryBag_Tbls.Add(laundryBag_Tbl);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = laundryBag_Tbl.Id }, laundryBag_Tbl);
        }

        // DELETE: api/LaundryBag_Tbl/5
        [ResponseType(typeof(LaundryBag_Tbl))]
        public async Task<IHttpActionResult> DeleteLaundryBag_Tbl(int id)
        {
            LaundryBag_Tbl laundryBag_Tbl = await db.LaundryBag_Tbls.FindAsync(id);
            if (laundryBag_Tbl == null)
            {
                return NotFound();
            }

            db.LaundryBag_Tbls.Remove(laundryBag_Tbl);
            await db.SaveChangesAsync();

            return Ok(laundryBag_Tbl);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LaundryBag_TblExists(int id)
        {
            return db.LaundryBag_Tbls.Count(e => e.Id == id) > 0;
        }
    }
}