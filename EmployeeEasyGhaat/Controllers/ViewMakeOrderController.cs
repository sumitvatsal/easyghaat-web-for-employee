﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Data.SqlClient;


namespace EmployeeEasyGhaat.Controllers
{
    public class ViewMakeOrderController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        [HttpGet]
        [ActionName("MakeOrder")]
        //GET:api/ViewMakeOrder/MakeOrder/TaskId
        public async Task<IHttpActionResult> MakeOrder(int TaskId )
        {
           
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(Connection.connstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_VIEWMAKEORDER", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TASKID", TaskId);
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                dt.Load(reader);
            }
            if ((dt == null) || (dt.Rows.Count == 0))
            {
                con.Close();
                return NotFound();
            }
            con.Close();
            return Ok(dt);
        }





        [HttpGet]
        [ActionName("MakeOrderDetail")]
        //GET:api/ViewMakeOrder/MakeOrderDetail/OrderId
        public async Task<IHttpActionResult> MakeOrderDetail(int OrderId)
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(Connection.connstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_GetOrderDetail", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrderId", OrderId);
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                dt.Load(reader);
            }
            if ((dt == null) || (dt.Rows.Count == 0))
            {
                con.Close();
                return NotFound();
            }
            con.Close();
            return Ok(dt);
        }




    }
}
