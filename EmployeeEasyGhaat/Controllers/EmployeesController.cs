﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Data.SqlClient;

namespace EmployeeEasyGhaat.Controllers
{
    public class EmployeesController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // GET: api/Employees/GetEmployeeByEmail/EmailId/password/
        public IQueryable<Employee> GetEmployeeByEmail( string EmailId, string password )
        {
            return db.Employees.Where(e => e.EmailId == EmailId && e.password == password && e.IsActive == true && e.Isdeleted==false);
        }

        // GET: api/Employees/GetEmployeeByPhoneNo/PhoneNo/password/
        public IQueryable<Employee> GetEmployeeByPhoneNo(string PhoneNo, string password)
        {
            return db.Employees.Where(e => e.per_phoneNo == PhoneNo && e.password == password && e.IsActive == true && e.Isdeleted == false);
        }

        // GET: api/Employees/GetEmployeePasswordByMobileNumber/
        public IQueryable<Employee> GetEmployeePasswordByMobileNumber(string PhoneNo, int emp_id)
        {
            return db.Employees.Where(e => e.emp_id == emp_id && e.per_phoneNo==PhoneNo);
        }

        // GET: api/Employees/5
        [ResponseType(typeof(Employee))]
        public async Task<IHttpActionResult> GetEmployee(int id)
        {
            Employee employee = await db.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            return Ok(employee);
        }

        // PUT: api/Employees/PutEmployeePassword/emp_id/password
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEmployeePassword(int emp_id, string password)
        {
            Employee employee = new Employee();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (emp_id == 0)
            {
                return BadRequest();
            }

            employee = db.Employees.Find(emp_id);
            try
            {
             if (employee != null)
            {
                employee.emp_id = emp_id;
                employee.password = password;
                db.Entry(employee).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Ok("SuccessFully Changed");
            }
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(emp_id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
          } 
        // POST: api/Employees
        [ResponseType(typeof(Employee))]
        public async Task<IHttpActionResult> PostEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Employees.Add(employee);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = employee.emp_id }, employee);
        }

        // DELETE: api/Employees/5
        [ResponseType(typeof(Employee))]
        public async Task<IHttpActionResult> DeleteEmployee(int id)
        {
            Employee employee = await db.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            db.Employees.Remove(employee);
            await db.SaveChangesAsync();

            return Ok(employee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.emp_id == id) > 0;
        }
    }
}