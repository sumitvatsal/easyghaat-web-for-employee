﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Data.SqlClient;
namespace EmployeeEasyGhaat.Controllers
{
    public class ViewInstantServicesController : ApiController
    {


        [HttpGet]
        [ActionName("ServiceRequired")]
        //GET:api/ViewInstantServices/ServiceRequired/OrderId
        public async Task<IHttpActionResult> ServiceRequired(int OrderId)
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(Connection.connstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("Sp_ServiceRequired", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrderId", OrderId);
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                dt.Load(reader);
            }
            if ((dt == null) || (dt.Rows.Count == 0))
            {
                con.Close();
                return NotFound();
            }
            con.Close();
            return Ok(dt);
        }
    }
}
