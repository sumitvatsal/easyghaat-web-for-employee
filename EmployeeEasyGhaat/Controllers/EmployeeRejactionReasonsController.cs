﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;

namespace EmployeeEasyGhaat.Controllers
{
    public class EmployeeRejactionReasonsController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // GET:api/EmployeeRejactionReasons/GetEmployeeRejactionReasons
        public IQueryable<EmployeeRejactionReason> GetEmployeeRejactionReasons()
        {
            return db.EmployeeRejactionReasons;
        }

        // GET: api/EmployeeRejactionReasons/5
        [ResponseType(typeof(EmployeeRejactionReason))]
        public async Task<IHttpActionResult> GetEmployeeRejactionReason(int id)
        {
            EmployeeRejactionReason employeeRejactionReason = await db.EmployeeRejactionReasons.FindAsync(id);
            if (employeeRejactionReason == null)
            {
                return NotFound();
            }

            return Ok(employeeRejactionReason);
        }

        // PUT: api/EmployeeRejactionReasons/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEmployeeRejactionReason(int id, EmployeeRejactionReason employeeRejactionReason)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employeeRejactionReason.Id)
            {
                return BadRequest();
            }

            db.Entry(employeeRejactionReason).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeRejactionReasonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EmployeeRejactionReasons
        [ResponseType(typeof(EmployeeRejactionReason))]
        public async Task<IHttpActionResult> PostEmployeeRejactionReason(EmployeeRejactionReason employeeRejactionReason)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EmployeeRejactionReasons.Add(employeeRejactionReason);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = employeeRejactionReason.Id }, employeeRejactionReason);
        }

        // DELETE: api/EmployeeRejactionReasons/5
        [ResponseType(typeof(EmployeeRejactionReason))]
        public async Task<IHttpActionResult> DeleteEmployeeRejactionReason(int id)
        {
            EmployeeRejactionReason employeeRejactionReason = await db.EmployeeRejactionReasons.FindAsync(id);
            if (employeeRejactionReason == null)
            {
                return NotFound();
            }

            db.EmployeeRejactionReasons.Remove(employeeRejactionReason);
            await db.SaveChangesAsync();

            return Ok(employeeRejactionReason);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeRejactionReasonExists(int id)
        {
            return db.EmployeeRejactionReasons.Count(e => e.Id == id) > 0;
        }
    }
}