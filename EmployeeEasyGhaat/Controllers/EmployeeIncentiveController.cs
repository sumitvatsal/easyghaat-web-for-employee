﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Data.SqlClient;
using System.Collections;

namespace EmployeeEasyGhaat.Controllers
{
    public class EmployeeIncentiveController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        SqlConnection con = new SqlConnection(Connection.connstring);

        [Route("api/EmployeeIncentive/ByTaskID/{TaskID}")]
        [HttpGet]
        public int AddingEmployeeIncentive(int TaskID)
        {
            //Response Received through status Code:
            //200 => OK
            //400 => Error(Error Occurred)
            int status = 0;
            var TaskAssign_Inct_dtls = db.TaskAssign_Incent_views.SingleOrDefault(t => t.TaskID == TaskID);
            if (TaskAssign_Inct_dtls != null)
            {
                var Emp_Inct_dtls = db.EmployeeIncentivePetrol_tbls.SingleOrDefault(e => e.BranchId == TaskAssign_Inct_dtls.BranchId && e.RoleTypeId == TaskAssign_Inct_dtls.RoleTypeId && e.IsActive == true && e.IsDeleted == false);
                if (Emp_Inct_dtls != null)
                {
                    double Petrol = Emp_Inct_dtls.PetrolAmount;
                    double Incentive = Emp_Inct_dtls.IncentiveAmount;
                    //PickUp
                    if (TaskAssign_Inct_dtls.TaskTypeId == 1)
                    {
                        status=PickUp(TaskAssign_Inct_dtls.TaskID, TaskAssign_Inct_dtls.OrderId, Petrol, Incentive);
                    }
                    //Delivery
                    else if (TaskAssign_Inct_dtls.TaskTypeId == 2)
                    {
                        status = Delivery(TaskAssign_Inct_dtls.TaskID, TaskAssign_Inct_dtls.OrderId, Petrol, Incentive);
                    }
                }
                else
                {
                    status = 400;
                }
            }
            else
            {
                status = 400;
            }
            return status;
        }

        public int PickUp(int TaskId,int OrderId,double Petrol_amt,double Incentive_amt)
        {
            int status = 0;
            DateTime Pkuptbl_Datetime, Pkupord_Datetime;
            var PickUp_dtls = db.PickUp_tbls.SingleOrDefault(p => p.TaskId == TaskId);
            if (PickUp_dtls != null)
            {
                //D
                Pkuptbl_Datetime = PickUp_dtls.PickUpDate;
                var Order_tbl = db.Order_tbls.SingleOrDefault(o => o.Id == OrderId);
                if (Order_tbl != null)
                {
                    long pckupSlotId = Convert.ToInt64(Order_tbl.PickUpTimeSlotId);
                    //F
                    Pkupord_Datetime = Convert.ToDateTime(Order_tbl.PickUpDate);
                    var Timeslot = db.TimeSlots.SingleOrDefault(tm => tm.id == pckupSlotId);
                    string frm_tme = Timeslot.FromTime;
                    string to_tme = Timeslot.ToTime;

                    float pck_time = Pkuptbl_Datetime.Hour;
                    float pck_min = Pkuptbl_Datetime.Minute / 60;
                    float pck_sec = Pkuptbl_Datetime.Second / 360;
                    pck_time += pck_min + pck_sec;

                    string[] fr_slot_tme = frm_tme.Split(':');
                    float ord_frmhr = Convert.ToSingle(fr_slot_tme[0]);
                    float ord_frmmin = Convert.ToSingle(fr_slot_tme[1])/60;
                    ord_frmhr += ord_frmmin;


                    string[] to_slot_tme = to_tme.Split(':');
                    float ord_tohr = Convert.ToSingle(to_slot_tme[0]);
                    float ord_tomin = Convert.ToSingle(to_slot_tme[1]) / 60;
                    ord_tohr += ord_tomin;

                    Pkuptbl_Datetime =Convert.ToDateTime(Pkuptbl_Datetime.ToShortDateString());
                    //Check D in B/W F
                    if(Pkuptbl_Datetime<= Pkupord_Datetime)
                    {
                        //YES
                        if(pck_time>= ord_frmhr && pck_time<= ord_tohr)
                        {
                            status = Yes_Case(TaskId, Petrol_amt,Incentive_amt);
                        }
                        //No
                        else
                        {
                            status = NO_Case(TaskId, Petrol_amt);
                        }
                    }
                    //NO
                    else
                    {
                        status = NO_Case(TaskId, Petrol_amt);
                    }
                }
                else
                {
                    status = 400;
                }
            }
            else
            {
                status = 400;
            }
            return status;
        }

        public int Delivery(int TaskId, int OrderId, double Petrol_amt, double Incentive_amt)
        {
            int status = 0;
            DateTime Dly_tbl_Datetime, dlyord_Datetime;
            var Dly_dtls = db.OrderDelivery_tbls.SingleOrDefault(p => p.TaskId == TaskId);
            if (Dly_dtls != null)
            {
                //D
                Dly_tbl_Datetime = Dly_dtls.DeliveryDate;
                var Order_tbl = db.Order_tbls.SingleOrDefault(o => o.Id == OrderId);
                if (Order_tbl != null)
                {
                    long dlySlotId = Convert.ToInt64(Order_tbl.DeliveryTimeSlotId);
                    //F
                    dlyord_Datetime = Convert.ToDateTime(Order_tbl.DeliveryDate);
                    var Timeslot = db.TimeSlots.SingleOrDefault(tm => tm.id == dlySlotId);
                    string frm_tme = Timeslot.FromTime;
                    string to_tme = Timeslot.ToTime;

                    float dly_time = Dly_tbl_Datetime.Hour;
                    float dly_min = Dly_tbl_Datetime.Minute / 60;
                    float dly_sec = Dly_tbl_Datetime.Second / 360;
                    dly_time += dly_min + dly_sec;

                    string[] fr_slot_tme = frm_tme.Split(':');
                    float ord_frmhr = Convert.ToSingle(fr_slot_tme[0]);
                    float ord_frmmin = Convert.ToSingle(fr_slot_tme[1]) / 60;
                    ord_frmhr += ord_frmmin;


                    string[] to_slot_tme = to_tme.Split(':');
                    float ord_tohr = Convert.ToSingle(to_slot_tme[0]);
                    float ord_tomin = Convert.ToSingle(to_slot_tme[1]) / 60;
                    ord_tohr += ord_tomin;

                    Dly_tbl_Datetime = Convert.ToDateTime(Dly_tbl_Datetime.ToShortDateString());
                    //Check D in B/W F
                    if (Dly_tbl_Datetime <= dlyord_Datetime)
                    {
                        //YES
                        if (dly_time >= ord_frmhr && dly_time <= ord_tohr)
                        {
                            status = Yes_Case(TaskId, Petrol_amt, Incentive_amt);
                        }
                        //No
                        else
                        {
                            status = NO_Case(TaskId, Petrol_amt);
                        }
                    }
                    //NO
                    else
                    {
                        status = NO_Case(TaskId, Petrol_amt);
                    }
                }
                else
                {
                    status = 400;
                }
            }
            else
            {
                status = 400;
            }
            return status;
        }
        public int NO_Case(int TaskId, double Petrol_amount)
        {
            int status = 0;
            EmpIncentivePetrolDetail_Tbl emp = new EmpIncentivePetrolDetail_Tbl();
            emp.TaskId = TaskId;
            emp.IncentiveAmount = 0;
            emp.PetrolAmount = Petrol_amount;
            emp.IsTransferred = false;
            emp.IsActive = true;
            emp.IsDeleted = false;
            emp.CreateDate = Connection.indianTime;
            if(!ModelState.IsValid)
            {
                status = 400;
                return status;
            }
            db.EmpIncentivePetrolDetail_Tbls.Add(emp);
            db.SaveChanges();
            status = 200;
            return status;
        }

        public int Yes_Case(int TaskId, double Petrol_amount, double Incentive_amount)
        {
            int status = 0;
            EmpIncentivePetrolDetail_Tbl emp = new EmpIncentivePetrolDetail_Tbl();
            emp.TaskId = TaskId;
            emp.IncentiveAmount = Incentive_amount;
            emp.PetrolAmount = Petrol_amount;
            emp.IsTransferred = false;
            emp.IsActive = true;
            emp.IsDeleted = false;
            emp.CreateDate = Connection.indianTime;
            if (!ModelState.IsValid)
            {
                status = 400;
                return status;
            }
            db.EmpIncentivePetrolDetail_Tbls.Add(emp);
            db.SaveChanges();
            status = 200;
            return status;
        }

        [Route("api/EmployeeIncentive/UpdateMyPackge/{OrderId}")]
        [HttpGet]
        public int UpdateMyPackge(int OrderId)
        {
            int status = 0;
            var pkg_trans = db.Package_Transctn_tbls.Where(pk => pk.OrderId == OrderId);
            if(pkg_trans!=null)
            {
                int pkg_bal = pkg_trans.Sum(p => p.AdjustedCount);
                var Order_dtl = db.Order_tbls.SingleOrDefault(o => o.Id == OrderId);
                int CustomerId =Convert.ToInt32(Order_dtl.CustomerId);
                status = Update_pkgbalance(pkg_bal, CustomerId);
            }

            return status;
        }


        public int Update_pkgbalance(int pkg_bal,int CustomerId)
        {
            int status = 0;
            SqlConnection con = new SqlConnection(Connection.connstring);
            SqlCommand cmd = new SqlCommand("sp_Update_Make_My_Package", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CustomerId", CustomerId);
            cmd.Parameters.AddWithValue("@Balance", pkg_bal);            
            try
            {

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                status = 200;

            }
            catch (Exception ex)
            {
                con.Close();
                cmd.Dispose();
                status = 400;

            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return status;
        }


        //Deleting the CustomerInvoice by passing ByOrderId
        [Route("api/EmployeeIncentive/DeleteInvoice/ByOrderId/{OrderId}")]
        [HttpGet]
        public int DeleteInvoice(int OrderId)
        {
            int status = 0;           
            SqlCommand cmd = new SqlCommand("sp_Cancel_Invoice", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrderId", OrderId);          
            try
            {

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                status = 200;

            }
            catch (Exception ex)
            {
                con.Close();
                cmd.Dispose();
                status = 400;

            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return status;
        }


        [Route("api/EmployeeIncentive/GetVendorHandoverTask/ByEmpId/{EmpId}")]
        [HttpGet]
        public IHttpActionResult GetVendorHandoverTask(int EmpId)
        {
            DataTable data = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SP_GetVendorHandoverTask", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpId", EmpId);
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataSet ds = new DataSet();//Dataset is collection of Tables 
                adp.Fill(ds, "HandoverTask");//here fill table with HandoverTask name in Dataset
                data = ds.Tables["HandoverTask"];
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch(Exception ex)
            {
                return NotFound();
            }
            
           
        }


        [Route("api/EmployeeIncentive/AddHandoverTask/ByPickUpId/{PickUpId}")]
        [HttpGet]
        public int AddHandoverTask(int PickUpId)
        {
            int status = 0, max_hrs=0;
            try
            {
                DataTable data = new DataTable();
                SqlCommand cmd = new SqlCommand("SP_GetVendorMaxDeliveryHours", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PickUpId", PickUpId);
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataSet ds = new DataSet();//Dataset is collection of Tables 
                adp.Fill(ds, "MaxDeliveryHours");//here fill table with MaxDeliveryHours name in Dataset
                data = ds.Tables["MaxDeliveryHours"];
                if (data.Rows.Count > 0)
                {
               
                 if (!(data.Rows[0]["MaxServiceHours"] is DBNull))
                    {
                    max_hrs = Convert.ToInt32(data.Rows[0]["MaxServiceHours"]);
                }
                else  //here adding 48 hrs if MaxServiceHours return null value
                {
                    max_hrs = 48;
                    
                }
                  
                }

                VendorHandOver_tbl vendor = new VendorHandOver_tbl();
                vendor.PickUpId = PickUpId;
                vendor.HandOverDate = Connection.indianTime;
                vendor.IsActive = true;
                vendor.IsDeleted = false;
                vendor.CreateDate = Connection.indianTime;
                vendor.ExpectedTakeOverTime = Connection.indianTime.AddHours(max_hrs);

                if (!ModelState.IsValid)
                {
                    status = 400;
                    return status;
                }
                db.VendorHandOver_tbls.Add(vendor);
                db.SaveChanges();
                status = 200;
            }
            catch(Exception ex)
            {
                status = 400;
            }
            return status;               

        }

        [Route("api/EmployeeIncentive/GetEmployeeCashPending/ByEmpId/{EmpId}")]
        [HttpGet]
        public IHttpActionResult GetEmployeeCashPending(int EmpId)
        {
            DataTable data = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SP_EmployeeCashPending", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpId", EmpId);
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataSet ds = new DataSet();//Dataset is collection of Tables 
                adp.Fill(ds, "EmployeeCashPending");//here fill table with HandoverTask name in Dataset
                data = ds.Tables["EmployeeCashPending"];
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return NotFound();
            }


        }


        [Route("api/EmployeeIncentive/AddCashhandover")]
        [HttpPost]
        public async Task<IHttpActionResult> PostCashhandover(Cashhandover Cashover)
        {
            //Response Received through status Code:
            //200 => OK(Add Successfully)            
            //400 => Error(Error Occurred)

            int status = 0; double tot_amount = 0;

            try
            {

                //here checking contain single Id or not
                if (Cashover.PendingIds.Contains(','))
                        {
                            // here passing all PendingIds & get all record realted with Ids,PendingIds pass in format of : 1,2,3
                            string[] PIds = Cashover.PendingIds.Split(',');
                            for (int i = 0; i < PIds.Length; i++)
                            {
                                int Id = Convert.ToInt32(PIds[i]);
                                var EmpCash_dtls = db.EmpCashReceiving_Tbls.SingleOrDefault(emp => emp.Id == Id);
                                if (EmpCash_dtls != null)
                                {
                                    tot_amount += EmpCash_dtls.TotalAmount;
                                }
                            }

                                            CashHandOver_Tbl cshbk_hdor = new CashHandOver_Tbl();
                                            cshbk_hdor.FromEmpId = Cashover.FromEmpId;
                                            cshbk_hdor.ToEmployeeId = Cashover.ToEmpId;
                                            cshbk_hdor.TotalAmount = tot_amount;
                                            cshbk_hdor.HandOverDate = Connection.indianTime;
                                            cshbk_hdor.IsSubmitted = false;
                                            cshbk_hdor.IsActive = true;
                                            cshbk_hdor.IsDeleted = false;
                                            cshbk_hdor.CreateDate = Connection.indianTime;

                                            if (!ModelState.IsValid)
                                            {
                                                status = 400;
                                                return BadRequest(ModelState);
                                            }

                                            db.CashHandOver_Tbls.Add(cshbk_hdor);
                                            await db.SaveChangesAsync();
                                            status = AddHandoverTask(PIds, cshbk_hdor.Id);
                         }
                        else
                        {
                            int Id = Convert.ToInt32(Cashover.PendingIds);
                            var EmpCash_dtls = db.EmpCashReceiving_Tbls.SingleOrDefault(emp => emp.Id == Id);
                            if (EmpCash_dtls != null)
                            {
                                tot_amount += EmpCash_dtls.TotalAmount;
                            }

                    CashHandOver_Tbl cshbk_hdor = new CashHandOver_Tbl();
                    cshbk_hdor.FromEmpId = Cashover.FromEmpId;
                    cshbk_hdor.ToEmployeeId = Cashover.ToEmpId;
                    cshbk_hdor.TotalAmount = tot_amount;
                    cshbk_hdor.HandOverDate = Connection.indianTime;
                    cshbk_hdor.IsSubmitted = false;
                    cshbk_hdor.IsActive = true;
                    cshbk_hdor.IsDeleted = false;
                    cshbk_hdor.CreateDate = Connection.indianTime;

                    if (!ModelState.IsValid)
                    {
                        status = 400;
                        return BadRequest(ModelState);
                    }

                    db.CashHandOver_Tbls.Add(cshbk_hdor);
                    await db.SaveChangesAsync();
                    status = AddHandoverTask_SingleId(Cashover.PendingIds, cshbk_hdor.Id);
                }
                

            }
            catch (Exception ex)
            {
                status = 400;
            }

            return Ok(status);
        }

        public int AddHandoverTask(string[] PIds,int HandoverId)
        {
            int status = 0;
            try
            {
                for (int i = 0; i < PIds.Length; i++)
                {
                    int Id = Convert.ToInt32(PIds[i]);
                    var EmpCash_dtls = db.EmpCashReceiving_Tbls.SingleOrDefault(emp => emp.Id == Id);
                    EmpCash_dtls.IsHandOver = true;
                    EmpCash_dtls.HandOverDate = Connection.indianTime;
                    EmpCash_dtls.HandoverId = HandoverId;
                    db.SaveChanges();
                }
                status = 200;
            }
            catch (Exception ex)
            {
                status = 400;
            }

            return status;
        }
        //method for pass single Id 
        public int AddHandoverTask_SingleId(string PIds, int HandoverId)
        {
            int status = 0;
            try
            {
                int Id = Convert.ToInt32(PIds);
                var EmpCash_dtls = db.EmpCashReceiving_Tbls.SingleOrDefault(emp => emp.Id == Id);
                EmpCash_dtls.IsHandOver = true;
                EmpCash_dtls.HandOverDate = Connection.indianTime;
                EmpCash_dtls.HandoverId = HandoverId;
                db.SaveChanges();
                status = 200;
            }
            catch (Exception ex)
            {
                status = 400;
            }

            return status;
        }

        [Route("api/EmployeeIncentive/GetEmployeeEarning/ByEmpId/{EmpId}")]
        [HttpGet]
        public IHttpActionResult GetEmployeeEarning(int EmpId)
        {
            DataTable data = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SP_GetEmployeeEarning", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeId ", EmpId);
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataSet ds = new DataSet();//Dataset is collection of Tables 
                adp.Fill(ds, "EmployeeEarning");//here fill table with EmployeeEarning name in Dataset
                data = ds.Tables["EmployeeEarning"];
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return NotFound();
            }


        }

        [Route("api/EmployeeIncentive/UpdateEmployeeSecretPin/ByEmpId/{EmpId}/{SecretPin}")]
        [HttpGet]
        public int UpdateEmployeeSecretPin(int EmpId, int SecretPin)
        {
            int status = 0;
            try
            {
                var Emp_dtls = db.Employees.SingleOrDefault(emp => emp.emp_id == EmpId);
                Emp_dtls.SecretPin = SecretPin;
                db.SaveChanges();               
                status = 200;
            }
            catch (Exception ex)
            {
                status = 400;
            }

            return status;
        }

        [Route("api/EmployeeIncentive/GetEmployeeEstatement")]
        [HttpPost]
        public IHttpActionResult GetEmployeeEstatement(EmployeeEstatement EmpStat)
        {
            DataTable data = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("SP_GetEmployeeEstatement", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeId ", EmpStat.EmployeeId);
                cmd.Parameters.AddWithValue("@FromDate ", EmpStat.FromDate);
                cmd.Parameters.AddWithValue("@ToDate ", EmpStat.ToDate);
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataSet ds = new DataSet();//Dataset is collection of Tables 
                adp.Fill(ds, "EmployeeEstatement");//here fill table with EmployeeEarning name in Dataset
                data = ds.Tables["EmployeeEstatement"];
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return NotFound();
            }


        }
    }
}
