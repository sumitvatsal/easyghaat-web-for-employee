﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;


namespace EmployeeEasyGhaat.Controllers
{
    public class CancelReasonsController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // GET: api/CancelReasons/RoleId
        public IQueryable<CancelReason> GetCancelReasons(int RoleId)
        {
            return db.CancelReasons.Where(rd => rd.RoleId == RoleId);
        }

        // GET: api/CancelReasons/5
        [ResponseType(typeof(CancelReason))]
        public async Task<IHttpActionResult> GetCancelReason(int id)
        {
            CancelReason cancelReason = await db.CancelReasons.FindAsync(id);
            if (cancelReason == null)
            {
                return NotFound();
            }

            return Ok(cancelReason);
        }


        

        // PUT: api/CancelReasons/PutUpdateOrderStatus/OrderId
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUpdateOrderStatus(int OrderId)
        {
            Order_tbl Order_tbl = new Order_tbl();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (OrderId == 0)
            {
                return BadRequest();
            }
            Order_tbl = db.Order_tbls.Where(a => a.Id == OrderId).SingleOrDefault();
            try
            {
                if (Order_tbl != null)
                {
                    Order_tbl.Id = OrderId;
                    Order_tbl.OrderStatusId = 7;
                    db.Entry(Order_tbl).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return Ok("Cancel Done");
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CancelReasonExists(OrderId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CancelReasons
        [ResponseType(typeof(CancelReason))]
        public async Task<IHttpActionResult> PostCancelReason(CancelReason cancelReason)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CancelReasons.Add(cancelReason);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = cancelReason.Id }, cancelReason);
        }

        // DELETE: api/CancelReasons/5
        [ResponseType(typeof(CancelReason))]
        public async Task<IHttpActionResult> DeleteCancelReason(int id)
        {
            CancelReason cancelReason = await db.CancelReasons.FindAsync(id);
            if (cancelReason == null)
            {
                return NotFound();
            }

            db.CancelReasons.Remove(cancelReason);
            await db.SaveChangesAsync();

            return Ok(cancelReason);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CancelReasonExists(int id)
        {
            return db.CancelReasons.Count(e => e.Id == id) > 0;
        }
    }
}