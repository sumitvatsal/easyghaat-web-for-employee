﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;

namespace EmployeeEasyGhaat.Controllers
{
    public class Emp_InfoController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // GET: api/Emp_Info
        public IQueryable<Emp_Info> GetEmp_Infos()
        {
            return db.Emp_Infos;
        }



        // GET: api/Emp_Info/GetEmployeeByEmail/EmailId/password/
        public IQueryable<Emp_Info> GetEmployeeByEmail(string EmailId, string password)
        {
            return db.Emp_Infos.Where(e => e.EmailId == EmailId && e.password == password && e.IsActive == true && e.Isdeleted == false);
        }

        // GET: api/Emp_Info/GetEmployeeByPhoneNo/PhoneNo/password/
        public IQueryable<Emp_Info> GetEmployeeByPhoneNo(string PhoneNo, string password)
        {
            return db.Emp_Infos.Where(e => e.per_phoneNo == PhoneNo && e.password == password && e.IsActive == true && e.Isdeleted == false);
        }

        // GET: api/Emp_Info/GetEmployeePasswordByMobileNumber/
        public IQueryable<Emp_Info> GetEmployeePasswordByMobileNumber(string PhoneNo, int emp_id)
        {
            return db.Emp_Infos.Where(e => e.emp_id == emp_id && e.per_phoneNo == PhoneNo);
        }




        // GET: api/Emp_Info/5
        [ResponseType(typeof(Emp_Info))]
        public async Task<IHttpActionResult> GetEmp_Info(int id)
        {
            Emp_Info emp_Info = await db.Emp_Infos.FindAsync(id);
            if (emp_Info == null)
            {
                return NotFound();
            }

            return Ok(emp_Info);
        }

        // PUT: api/Emp_Info/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEmp_Info(int id, Emp_Info emp_Info)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != emp_Info.emp_id)
            {
                return BadRequest();
            }

            db.Entry(emp_Info).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Emp_InfoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Emp_Info
        [ResponseType(typeof(Emp_Info))]
        public async Task<IHttpActionResult> PostEmp_Info(Emp_Info emp_Info)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Emp_Infos.Add(emp_Info);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = emp_Info.emp_id }, emp_Info);
        }

        // DELETE: api/Emp_Info/5
        [ResponseType(typeof(Emp_Info))]
        public async Task<IHttpActionResult> DeleteEmp_Info(int id)
        {
            Emp_Info emp_Info = await db.Emp_Infos.FindAsync(id);
            if (emp_Info == null)
            {
                return NotFound();
            }

            db.Emp_Infos.Remove(emp_Info);
            await db.SaveChangesAsync();

            return Ok(emp_Info);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Emp_InfoExists(int id)
        {
            return db.Emp_Infos.Count(e => e.emp_id == id) > 0;
        }
    }
}