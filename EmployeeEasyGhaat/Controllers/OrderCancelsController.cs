﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;


namespace EasyGhaat.Areas.Vendordashboard.Controllers
{
    public class OrderCancelsController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // GET: api/OrderCancels
        public IQueryable<OrderCancel> GetOrderCancels()
        {
            return db.OrderCancels;
        }

        // GET: api/OrderCancels/5
        [ResponseType(typeof(OrderCancel))]
        public async Task<IHttpActionResult> GetOrderCancel(int id)
        {
            OrderCancel orderCancel = await db.OrderCancels.FindAsync(id);
            if (orderCancel == null)
            {
                return NotFound();
            }

            return Ok(orderCancel);
        }

        // PUT: api/OrderCancels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrderCancel(int id, OrderCancel orderCancel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != orderCancel.Id)
            {
                return BadRequest();
            }

            db.Entry(orderCancel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderCancelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/OrderCancels/InsertCancelOrder/EmployeeId/CancelReasonId/OrderId/RoleTypeId
        [ResponseType(typeof(OrderCancel))]
        public async Task<IHttpActionResult> InsertCancelOrder(int EmployeeId, int CancelReasonId, int OrderId, int RoleTypeId)
        {
            OrderCancel orderCancel = new OrderCancel();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            orderCancel.CancelById = EmployeeId;
            orderCancel.ReasonId = CancelReasonId;
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
            orderCancel.CreateDate = indianTime;
            orderCancel.OrderId = OrderId;
            orderCancel.RoleTypeId = RoleTypeId;
            db.OrderCancels.Add(orderCancel);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = orderCancel.Id }, orderCancel);
        }

        // DELETE: api/OrderCancels/5
        [ResponseType(typeof(OrderCancel))]
        public async Task<IHttpActionResult> DeleteOrderCancel(int id)
        {
            OrderCancel orderCancel = await db.OrderCancels.FindAsync(id);
            if (orderCancel == null)
            {
                return NotFound();
            }

            db.OrderCancels.Remove(orderCancel);
            await db.SaveChangesAsync();

            return Ok(orderCancel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderCancelExists(int id)
        {
            return db.OrderCancels.Count(e => e.Id == id) > 0;
        }
    }
}