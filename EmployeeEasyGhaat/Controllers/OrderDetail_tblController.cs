﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Data.SqlClient;
using System.Collections;

namespace EmployeeEasyGhaat.Controllers
{
    public class OrderDetail_tblController : ApiController
    {
      
        SqlConnection con = new SqlConnection(Connection.connstring);
        private EmployeeContext db = new EmployeeContext();

        // GET: api/OrderDetail_tbl
        public IQueryable<OrderDetail_tbl> GetOrderDetail_tbls()
        {
            return db.OrderDetail_tbls;
        }       


        // GET: api/OrderDetail_tbl/5
        [ResponseType(typeof(OrderDetail_tbl))]
        public async Task<IHttpActionResult> GetOrderDetail_tbl(int id)
        {
            OrderDetail_tbl orderDetail_tbl = await db.OrderDetail_tbls.FindAsync(id);
            if (orderDetail_tbl == null)
            {
                return NotFound();
            }

            return Ok(orderDetail_tbl);
        }
             

        [Route("api/OrderDetail_tbl/UpdateOrderQty/{Id}/{Quantity}")]
        [HttpGet]
        public int UpdateOrderQty(int Id,int Quantity)
        {
            int status = 0;       
            SqlConnection con = new SqlConnection(Connection.connstring);
            SqlCommand cmd = new SqlCommand("sp_UpdateOrderDtls", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Qunatity", Quantity);
            cmd.Parameters.AddWithValue("@Id", Id);
            try
            {

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                return status = 200;
            }
            catch (Exception ex)
            {
                con.Close();
                cmd.Dispose();
                return status = 500;
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
           
        }


        [Route("api/OrderDetail_tbl/GenerateInvoice/{OrderId}")]
        [HttpGet]
        [ResponseType(typeof(CustomerInvoice_Tbl))]
        public async Task<IHttpActionResult> GenerateInvoice(int OrderId)
        {
                       
            double Ord_dts_tot_sub_amt = 0, pck_adjst_tot=0,p_and_D_min=0, p_and_D_dif=0,
                Get_surge_pcnt=0,surg_amt=0,min_ord_cpn=0, casb_bck=0, WalletBal=0,WalletAdjustment =0, NetBalance=0;
            try
            {
                Order_tbl Order_dt = db.Order_tbls.SingleOrDefault(od => od.Id == OrderId);
                if (Order_dt != null)
                {
                    Ord_dts_tot_sub_amt = Sum_Orderdtls(OrderId);
                }


                //GET Taxes Details
                var Tax_dtls = db.Tax_tbls.Where(tx => tx.IsActive == true && tx.IsDeleted == false);

                //GET Order_LaundryBag Details
                var Ord_lundry_dtls = db.Order_LaundryBag_Tbls.Where(tx => tx.OrderId == Order_dt.Id && tx.IsActive == true && tx.IsDeleted == false);

                //GET p & D MinimumOrderAmount
                var P_D_dtls = db.MinmumOrder_Value_Tbls.SingleOrDefault(p => p.BranchId == Order_dt.BranchId && p.IsActive == true && p.IsDeleted == false);
                if (P_D_dtls != null)
                {
                    p_and_D_min = P_D_dtls.MinimumOrderAmount;
                }


                //GET SurgerPercent
                var Surg_dtls = db.DeliveryType_SurgePrices.SingleOrDefault(s => s.DeliveryTypeId == Order_dt.DeliveryTypeId && s.BranchId == Order_dt.BranchId && s.IsActive == true && s.IsDeleted == false);

                if (Surg_dtls != null)
                {
                    Get_surge_pcnt = Surg_dtls.SurgePercentage;
                }


                bool use_pack_bal = Convert.ToBoolean(Order_dt.UsedPkgBalance);
                if (use_pack_bal)
                {

                    var pack_dtls = db.Make_My_Packages.SingleOrDefault(pk => pk.CustomerId == Order_dt.CustomerId);
                    int pack_balance = (int)pack_dtls.Balance;
                    if (pack_balance > 0)
                    {

                        SqlCommand cmd = new SqlCommand("sp_OrderDetail_tbl", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@OrderId", Order_dt.Id);
                        cmd.Parameters.AddWithValue("@CategoryId", 1);//here CategoryId=1 means Regular
                        cmd.Parameters.AddWithValue("@ServiceId", 1);//here ServiceId=1 means Wash & Iron
                        SqlDataAdapter adp = new SqlDataAdapter();
                        adp.SelectCommand = cmd;
                        DataSet ds = new DataSet();//Dataset is collection of Tables 
                        adp.Fill(ds, "OrderDetails");//here fill table with OrderDetails name in Dataset
                        foreach (DataRow row in ds.Tables["OrderDetails"].Rows)  //here reading the Datatable by name inside dataset
                        {
                            //Saving into Package_Transctn_tbl table
                            Package_Transctn_tbl pkg_trns = new Package_Transctn_tbl();


                            pkg_trns.OrderId = Convert.ToInt32(row["OrderId"]);
                            pkg_trns.PackageID = pack_dtls.Id;
                            pkg_trns.OrderDetailID = Convert.ToInt32(row["Id"]);
                            int O_dts_qty = Convert.ToInt32(row["Qunatity"]);

                            //find minimum b/w package balance & Quantity
                            int min_count = Math.Min(pack_balance, O_dts_qty);
                            pkg_trns.AdjustedCount = min_count;

                            //calculate closing balance
                            pack_balance -= O_dts_qty;
                            if (pack_balance <= 0)
                            {
                                pack_balance = 0;

                            }
                            pkg_trns.ClosingBal = pack_balance;

                            //calculate AmountAdjustment
                            double O_dts_sb_amt = (double)row["SubAmount"];
                            double pack_adjst = (O_dts_sb_amt / O_dts_qty) * min_count;
                            pkg_trns.AmountAdjustment = pack_adjst;
                            pck_adjst_tot += pack_adjst;

                            ////if (!ModelState.IsValid)
                            ////{
                            ////    status = 400;
                            ////}
                            pkg_trns.IsActive = true;
                            pkg_trns.IsDeleted = false;
                            pkg_trns.CreateDate =  Connection.indianTime;
                            db.Package_Transctn_tbls.Add(pkg_trns);
                            db.SaveChanges();
                            if (pack_balance == 0)
                                break;

                        }
                        //updating package balance
                        Update_MyPackage(pack_dtls.Id, pack_balance);

                    }

                }

                if (Ord_dts_tot_sub_amt < p_and_D_min)
                {
                    p_and_D_dif = p_and_D_min - Ord_dts_tot_sub_amt;
                }

                surg_amt = (Ord_dts_tot_sub_amt + p_and_D_dif) * Get_surge_pcnt / 100;
                min_ord_cpn = Ord_dts_tot_sub_amt + p_and_D_dif + surg_amt - pck_adjst_tot;


                //Find Cashback Price
                if (Order_dt.PromoCode.Length != 0)
                {
                    casb_bck = Cashback(min_ord_cpn, Order_dt, pck_adjst_tot);
                }

                //double cal_Amount = (Ord_dts_tot_sub_amt + p_and_D_dif + surg_amt) - pck_adjst_tot - casb_bck;
                double cal_Amount = (Ord_dts_tot_sub_amt + p_and_D_dif + surg_amt) - casb_bck;
                float Service_tax = 0, swach_bhrt_cess = 0, Kirsh_cess = 0;
                if (Tax_dtls != null)
                {
                    foreach (var taxs in Tax_dtls)
                    {
                        switch (taxs.Id)
                        {
                            //Service Tax
                            case 1:
                                Service_tax = (float)taxs.TaxPercentage;
                                break;

                            //Swach Bharat Abhiyan Cess                            
                            case 2:
                                swach_bhrt_cess = (float)taxs.TaxPercentage;
                                break;

                            //Krishi kalyan Cess                            
                            case 3:
                                Kirsh_cess = (float)taxs.TaxPercentage;
                                break;
                        }
                    }
                }


                //Calculating Service Tax
                double cal_service_tax = cal_Amount * Service_tax / 100;

                //Calculating Swach Bharat Abhiyan Cess  
                double cal_swach_bhrt_cess = cal_Amount * swach_bhrt_cess / 100;

                //Calculating Krishi kalyan Cess  
                double cal_krish_cess = cal_Amount * Kirsh_cess / 100;

                //Get Laundry Order Subamount
                int Ludry_sub_Amount = 0;
                if (Ord_lundry_dtls != null && Ord_lundry_dtls.Count()>0 )
                {
                    Ludry_sub_Amount = Ord_lundry_dtls.Sum(oly => oly.SubAmount);
                }


                double Total_Amount = (Ord_dts_tot_sub_amt + p_and_D_dif + surg_amt + cal_service_tax + cal_swach_bhrt_cess + cal_krish_cess + Ludry_sub_Amount) - pck_adjst_tot;
                double Total_Amount_roundoff = Math.Round(Total_Amount);
                double RoundOffAmount = Total_Amount_roundoff - Total_Amount;

                if (Convert.ToBoolean(Order_dt.UsedEsyWallet))
                {
                    var Wallet_dtls = db.Wallet_tbls.SingleOrDefault(w => w.CustomerId == Order_dt.CustomerId && w.IsActive == true && w.IsDeleted == false);
                    if (Wallet_dtls != null)
                    {
                        WalletBal = Wallet_dtls.Net_Amount;
                    }

                }
                WalletAdjustment = Math.Min(WalletBal, Total_Amount_roundoff);
                NetBalance = Total_Amount_roundoff - WalletAdjustment;

                CustomerInvoice_Tbl Cus_invoice = new CustomerInvoice_Tbl();
                Cus_invoice.OrderId = Order_dt.Id;
                Cus_invoice.InvoiceDateTime =  Connection.indianTime;
                Cus_invoice.SubAmount = Ord_dts_tot_sub_amt;
                Cus_invoice.PickUp_DeliveryCharges = p_and_D_dif;
                Cus_invoice.SurgingAmount = surg_amt;
                Cus_invoice.PackageAdjustmentAmt = pck_adjst_tot;
                Cus_invoice.LaundryBagAmount = Ludry_sub_Amount;
                Cus_invoice.CashBackAmount = casb_bck;
                Cus_invoice.ServiceTaxAmt = cal_service_tax;
                Cus_invoice.SwachhBharatCess = cal_swach_bhrt_cess;
                Cus_invoice.KrishiKalyanCessAmt = cal_krish_cess;
                Cus_invoice.TotalAmount = Total_Amount_roundoff;
                Cus_invoice.RoundOffAmount = RoundOffAmount;
                Cus_invoice.WalletAdjustment = WalletAdjustment;
                Cus_invoice.NetBalance = NetBalance;
                Cus_invoice.IsActive = true;
                Cus_invoice.IsDeleted = false;
                Cus_invoice.ModifiedDate =  Connection.indianTime;

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.CustomerInvoice_Tbls.Add(Cus_invoice);
                await db.SaveChangesAsync();
                return CreatedAtRoute("DefaultApi", new { controller = "OrderDetail_tbl", OrderId = Cus_invoice.OrderId }, Cus_invoice);
            }
            catch(Exception ex)
            {
                return BadRequest("400");
            }
        }

        public double Sum_Orderdtls(int OrderId)
        {
            double sum = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("sp_SumOrderdtls", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataSet ds = new DataSet();//Dataset is collection of Tables 
                adp.Fill(ds, "OrderDetails");//here fill table with OrderDetails name in Dataset
                DataTable dt = ds.Tables["OrderDetails"];
                DataRow dr = dt.Rows[0];
                sum = Convert.ToDouble(dr["Total"]);
            }
            catch(Exception ex)
            {

            }
          
            return sum;
        }

        public double Cashback(double min_ord_amt, Order_tbl ord_tbl,double pck_adjst_tot_final)
        {
           

                ArrayList Ord_SId = new ArrayList();
                ArrayList Prm_SId = new ArrayList();
                ArrayList Comm_SId = new ArrayList();
                double cash_bck = 0, Cash_bk_Amt = 0, pro_min_ord=0;
                string SId = "";
                try
                {
                var promo_code_dtls = db.PromoCodes.SingleOrDefault(p => p.Promo_Code == ord_tbl.PromoCode);
                if(promo_code_dtls!=null)
                {
                    pro_min_ord = promo_code_dtls.MinOrder;
                }
               
                if (min_ord_amt >= pro_min_ord)
                {
                    //Get Distinct ServiceId's from OrderDtls Table
                    var Ord_dtls_SId = db.OrderDetail_tbl_views.Where(or => or.OrderId == ord_tbl.Id).GroupBy(ord => ord.ServiceId).Select(grp => grp.FirstOrDefault());//here GroupBy() used for get distinct value of given column or FirstOrDefault() returns matching rows

                    //Get Distinct ServiceId's from PromoCode_Service Table
                    var promo_SId = db.Promo_Code_Services.Where(or => or.PromoCode_Id == promo_code_dtls.Id).GroupBy(ord => ord.ServiceId).Select(grp => grp.FirstOrDefault());//here GroupBy() used for get distinct value of given column or FirstOrDefault() returns matching rows

                    if(Ord_dtls_SId!=null)
                    {
                        foreach (var ord in Ord_dtls_SId)
                        {
                            Ord_SId.Add(ord.ServiceId);
                            // Ord_SId += ord.ServiceId + ",";
                        }
                    }
                   
                    if(promo_SId!=null)
                    {

                        foreach (var prm in promo_SId)
                        {
                            Prm_SId.Add(prm.ServiceId);
                        }
                    }


                    foreach (int o_sId in Ord_SId)
                    {
                        foreach (int p_sId in Prm_SId)
                        {
                            if (o_sId == p_sId)
                            {
                                Comm_SId.Add(o_sId);
                            }
                        }
                    }
                    Comm_SId.Sort();
                    if (Comm_SId.Count > 0)
                    {
                        SId = String.Join(",", Comm_SId.ToArray());//store all ids comma seprerated in variable

                        if (pck_adjst_tot_final > 0)
                        {
                            //calculate F & G
                            //F                                                                 //here CategoryId=1 means Regular, ServiceId=1 means wash & Iron
                            //var sum_data_f = db.OrderDetail_tbl_views.Where(o => o.OrderId == ord_tbl.Id && o.ServiceId == 1 && o.CategoryId == 1);
                            //double sum_F = sum_data_f.Sum(d => d.SubAmount);
                            //sum_F -= pck_adjst_tot_final;

                            //F & G   
                            var sum_data_G = db.OrderDetail_tbl_views.Where(o => o.OrderId == ord_tbl.Id);
                            double sum_G = 0, sum_F = 0;
                            if(sum_data_G!=null)
                            {
                                foreach (var data in sum_data_G)
                                {
                                    if (data.ServiceId == 1 && data.CategoryId == 1)
                                    {
                                        //F
                                        sum_F += data.SubAmount;
                                    }
                                    else
                                    {
                                        //G
                                        sum_G += data.SubAmount;
                                    }
                                }
                            }
                           
                            sum_F -= pck_adjst_tot_final;//here adjust pack_balance
                            Cash_bk_Amt = sum_F + sum_G;
                        }
                        else
                        {
                            //calculate H
                            var sum_data_H = db.OrderDetail_tbl_views.Where(o => o.OrderId == ord_tbl.Id && SId.Contains(o.ServiceId.ToString()));
                            double sum_H = 0;
                            if (sum_data_H!=null)
                            {
                                sum_H = sum_data_H.Sum(d => d.SubAmount);
                            }
                            
                            Cash_bk_Amt = sum_H;
                        }

                        //Calculate CashbackAmout
                        if(promo_code_dtls!=null)
                        {
                            //Percentage
                            if (promo_code_dtls.PromoType == 1)
                            {
                                int percent = Convert.ToInt32(promo_code_dtls.Percentage);
                                int max_dis = Convert.ToInt32(promo_code_dtls.MaxDiscount);

                                double cal_dis = Cash_bk_Amt * percent / 100;
                                if (cal_dis > max_dis)
                                {
                                    cash_bck = max_dis;
                                }
                                else
                                {
                                    cash_bck = cal_dis;
                                }
                            }
                            else //Flat
                            {
                                cash_bck = Convert.ToInt32(promo_code_dtls.FlatAmount);
                            }
                        }
                      

                    }
                }
            }
            catch(Exception ex)
            {

            }

            return Math.Round(cash_bck);
        }

    
        public void Update_MyPackage(int Id, int Balance)
        {
           
            SqlConnection con = new SqlConnection(Connection.connstring);
            SqlCommand cmd = new SqlCommand("sp_Update_MyPackage", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Balance", Balance);
            cmd.Parameters.AddWithValue("@Id", Id);
            try
            {

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
               
            }
            catch (Exception ex)
            {
                con.Close();
                cmd.Dispose();
               
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }

        }

        public int paymenttype = 0;
        [Route("api/OrderDetail_tbl/Confirm_Order/{TaskId}")]
        [HttpGet]
       // [ActionName("Confirm_Order")]
        // POST: api/OrderDetail_tbl/Confirm_Order
        public async Task<int> Confirm_Order(int TaskId)
        {
            //Response Received through status Code:
            //200 => OK
            //400 => Error(Error Occurred)

            int status = 0;//COD=1,COP=2

            var TaskAssignment = db.TaskAssignment_tbls.SingleOrDefault(cus => cus.Id == TaskId);
            if(TaskAssignment != null)
            {
                var Cus_Inv_wall = db.Cus_Invoice_Wallet_Orders.SingleOrDefault(cus => cus.OrderId == TaskAssignment.OrderId);
                if (Cus_Inv_wall.WalletAdjustment > 0)
                {
                    //update Wallet table
                  status=Add_wallet_trans(Cus_Inv_wall);
                }

                //Checking Net Balance Greater than zero or Not
                if(Cus_Inv_wall.NetBalance>0)
                {
                    if(Cus_Inv_wall.PaymentTypeId==null)
                    {
                        paymenttype = 1;
                    }
                    else
                    {
                        paymenttype = Convert.ToInt32(Cus_Inv_wall.PaymentTypeId);
                    }
                    status=Add_Pickupdate(TaskAssignment);
                }                
                else
                {
                    //Updating Payment Status as Paid
                    var Ord_dtls = db.Order_tbls.SingleOrDefault(o => o.Id == Cus_Inv_wall.OrderId);
                    if (Ord_dtls != null)
                    {
                        Ord_dtls.PaymentStatusId = 1;
                        db.SaveChanges();
                    }
                    status =Add_Pickupdate(TaskAssignment);
                    if(status==200)
                    {
                        status = Add_Cashbck(Cus_Inv_wall);
                    }
                }

                //Checking paymenttype
                if (paymenttype == 1)//COD
                {
                    CheckOrderFirstorNot(Cus_Inv_wall);
                }
                else if (paymenttype == 2) //COP
                {
                    status = Add_EmpCashReceiving(TaskAssignment.Id, Cus_Inv_wall);
                }

                //Updating Order Status to Picked              
                var Ord_dtls_pk = db.Order_tbls.SingleOrDefault(o => o.Id == Cus_Inv_wall.OrderId);
                if(Ord_dtls_pk!=null)
                {
                    Ord_dtls_pk.OrderStatusId = 3;// Picked=3
                    db.SaveChanges();
                }

            }
            else
            {
                status = 400;
            }

            return status;
        }

        public int Add_wallet_trans(Cus_Invoice_Wallet_Order Cus_Inv_wall)
        {
            int status = 0;
            Transaction_tbl transaction_tbl = new Models.Transaction_tbl();
            double walletBal = 0;
            walletBal = Cus_Inv_wall.Wallet_Amount - Cus_Inv_wall.WalletAdjustment;
            transaction_tbl.WalletId = Cus_Inv_wall.WalletId;
            transaction_tbl.Trans_Type = "Dr.";
            transaction_tbl.Amount = Cus_Inv_wall.WalletAdjustment;
            transaction_tbl.Closing_Bal = walletBal;
            transaction_tbl.OrderId = Cus_Inv_wall.OrderId;
            transaction_tbl.Trans_Date =  Connection.indianTime;
            transaction_tbl.CreateDate =  Connection.indianTime;
            transaction_tbl.IsActive = true;
            transaction_tbl.IsDeleted = false;
            db.Transaction_tbls.Add(transaction_tbl);
            db.SaveChanges();

            status=Update_wallet(walletBal, Cus_Inv_wall.WalletId);
            return status;
        }

        public int Add_Pickupdate(TaskAssignment_tbl tsk_Assign)
        {
            int status = 0;
            PickUp_tbl pkup = new PickUp_tbl();

            pkup.TaskId = tsk_Assign.Id;
            pkup.EmpId = tsk_Assign.EmpId;
            pkup.PickUpDate =  Connection.indianTime;
            pkup.IsActive = true;
            pkup.IsDeleted = false;
            pkup.CreateDate =  Connection.indianTime;
            db.PickUp_tbls.Add(pkup);
            db.SaveChanges();
            status = 200;
            return status;
        }
        public int Add_EmpCashReceiving(int TaskId, Cus_Invoice_Wallet_Order Cus_Inv_wall)
        {
            int status = 0;
            EmpCashReceiving_Tbl emp = new EmpCashReceiving_Tbl();
            emp.TaskId = TaskId;
            emp.TotalAmount = Cus_Inv_wall.NetBalance;
            emp.IsHandOver = false;
            emp.IsActive = true;
            emp.IsDeleted = false;
            emp.CreateDate = Connection.indianTime;

            if (!ModelState.IsValid)
            {
                status = 400;
                return status;
            }
            db.EmpCashReceiving_Tbls.Add(emp);            
            db.SaveChanges();

            if(Cus_Inv_wall.CashBackAmount>0)
            {
                status=Add_Cashbck(Cus_Inv_wall);
            }
            //Updating Payment Status as Paid
            var Ord_dtls = db.Order_tbls.SingleOrDefault(o => o.Id == Cus_Inv_wall.OrderId);
            if (Ord_dtls != null)
            {
                Ord_dtls.PaymentStatusId = 1;
                db.SaveChanges();
            }
            status = 200;
            return status;
        }
        public int Add_Cashbck(Cus_Invoice_Wallet_Order Cus_Inv_wall)
        {
            int status = 0;
            Cashback_tbl csh_bk = new Cashback_tbl();

            csh_bk.OrderId = Cus_Inv_wall.OrderId;
            csh_bk.Cashback_type = "Scehme";
            csh_bk.CustomerId = Cus_Inv_wall.CustomerId;
            csh_bk.Amount = Cus_Inv_wall.CashBackAmount;
            csh_bk.IsActive = true;
            csh_bk.IsDeleted = false;
            csh_bk.CreateDate = Connection.indianTime;
            csh_bk.Trans_Date = Connection.indianTime;
            if (!ModelState.IsValid)
            {
                status = 400;
                return status;
            }
            
            db.Cashback_tbls.Add(csh_bk);
            db.SaveChanges();
            status = 200;
            if (status==200)
            {
                status=Add_Cashbck_trans(Cus_Inv_wall,csh_bk.Id);
            }          
            return status;
        }
        public int Add_Cashbck_trans(Cus_Invoice_Wallet_Order Cus_Inv_wall,int CshId)
        {
            int status = 0;
            Transaction_tbl transaction_tbl = new Models.Transaction_tbl();
            double walletBal = 0;
            // walletBal = Cus_Inv_wall.Wallet_Amount + Cus_Inv_wall.CashBackAmount;

            //getting wallet exact amount
            var Wallet_tbl_dts = db.Wallet_tbls.SingleOrDefault(w => w.Id == Cus_Inv_wall.WalletId);
             walletBal = Wallet_tbl_dts.Net_Amount + Cus_Inv_wall.CashBackAmount;

            if(Wallet_tbl_dts != null)
            {
                transaction_tbl.WalletId = Cus_Inv_wall.WalletId;
                transaction_tbl.Trans_Type = "Cr.";
                transaction_tbl.Amount = Cus_Inv_wall.CashBackAmount;
                transaction_tbl.Closing_Bal = walletBal;
                transaction_tbl.CashbckId = CshId;
                transaction_tbl.OrderId = Cus_Inv_wall.OrderId;
                transaction_tbl.Trans_Date = Connection.indianTime;
                transaction_tbl.CreateDate = Connection.indianTime;
                transaction_tbl.IsActive = true;
                transaction_tbl.IsDeleted = false;
                if (!ModelState.IsValid)
                {
                    status = 400;
                    return status;
                }

                db.Transaction_tbls.Add(transaction_tbl);
                db.SaveChanges();
                status = 200;
                if (status == 200)
                {
                    status = Update_wallet(walletBal, Cus_Inv_wall.WalletId);
                    if (status == 200)
                    {
                        status = CheckOrderFirstorNot(Cus_Inv_wall);
                    }
                }
            }
           
                        
            return status;
        }

        //Checking OrderFirst Or NOt
        public int CheckOrderFirstorNot(Cus_Invoice_Wallet_Order Cus_Inv_wall)
        {
            int status = 0;
            int Ord_tbl = db.Order_tbls.Where(o => o.CustomerId == Cus_Inv_wall.CustomerId).Count();
            //Check First Order OR NOT
            if(Ord_tbl==1)
            {
                var Cus_Reg = db.Customer_Registers.SingleOrDefault(c => c.Id == Cus_Inv_wall.CustomerId && c.IsActive == true && c.IsDeleted == false);
                if(Cus_Reg!=null)
                {
                    string FrndReferalCode = Cus_Reg.FrndReferalCode;
                    var Cus_Referrer = db.Customer_Registers.SingleOrDefault(cus => cus.MyReferalCode == FrndReferalCode && cus.IsActive==true && cus.IsDeleted==false);
                    //    var Cus_Referrer = db.Customer_Registers.SingleOrDefault(cus => cus.MyReferalCode == FrndReferalCode);
                    if (Cus_Referrer!=null)
                    {
                        int Refer_CusId = Cus_Referrer.Id;
                        //Refer_Roleid == 1,Referrer
                     
                        var ReferalAmount = db.ReferalAmount_Tbls.SingleOrDefault(rf => rf.Refer_Roleid == 1 && rf.IsActive == true && rf.IsDeleted == false);
                        if (ReferalAmount!=null)
                        {
                            double Amount = ReferalAmount.Amount;
                            status = Add_ReferralCashbck(Refer_CusId, Amount, Cus_Inv_wall);
                        }

                    }
                }
                else
                {
                    //Updating Order Status : Payment Received or Order Placed
                    status = Update_Oder_tbl(Cus_Inv_wall.OrderId);
                }
                
            }
            else
            {
                //Updating Order Status : Payment Received or Order Placed
                status = Update_Oder_tbl(Cus_Inv_wall.OrderId);
            }
           
            return status;
        }

        public int Add_ReferralCashbck(int CustomerId,double Amount,Cus_Invoice_Wallet_Order Cus_Inv_wall)
        {
            int status = 0;
            Cashback_tbl csh_bk = new Cashback_tbl();
            
            csh_bk.Cashback_type = "Referrer";
            csh_bk.CustomerId = CustomerId;
            csh_bk.Amount = Amount;
            csh_bk.IsActive = true;
            csh_bk.IsDeleted = false;
            csh_bk.CreateDate = Connection.indianTime;
            csh_bk.Trans_Date = Connection.indianTime;
            if (!ModelState.IsValid)
            {
                status = 400;
                return status;
            }

            db.Cashback_tbls.Add(csh_bk);
            db.SaveChanges();
            status = 200;
            if (status == 200)
            {
                var Wall_dtl = db.Wallet_tbls.SingleOrDefault(w => w.CustomerId == CustomerId);
                if(Wall_dtl!=null)
                {
                    status = Add_ReferralCashbck_trans(Wall_dtl, csh_bk.Id, Amount, Cus_Inv_wall);
                }
                
            }
            return status;
        }
        public int Add_ReferralCashbck_trans(Wallet_tbl Wall_dtl, int CshId,double Amount, Cus_Invoice_Wallet_Order Cus_Inv_wall)
        {
            int status = 0;
            Transaction_tbl transaction_tbl = new Transaction_tbl();
            double walletBal = 0;
            walletBal = Wall_dtl.Net_Amount + Amount;
            transaction_tbl.WalletId = Wall_dtl.Id;
            transaction_tbl.Trans_Type = "Cr.";
            transaction_tbl.Amount = Amount;
            transaction_tbl.Closing_Bal = walletBal;
            transaction_tbl.CashbckId = CshId;           
            transaction_tbl.Trans_Date = Connection.indianTime;
            transaction_tbl.CreateDate = Connection.indianTime;
            transaction_tbl.IsActive = true;
            transaction_tbl.IsDeleted = false;
            if (!ModelState.IsValid)
            {
                status = 400;
                return status;
            }

            db.Transaction_tbls.Add(transaction_tbl);
            db.SaveChanges();
            status = 200;
            if (status == 200)
            {
                status = Update_wallet(walletBal, Wall_dtl.Id);
                if(status==200)
                {
                    status =Update_Oder_tbl(Cus_Inv_wall.OrderId);
                }                
            }
            return status;
        }

        public int Update_wallet(double walletBal,int WalletId)
        {
            int status = 0;
            SqlConnection con = new SqlConnection(Connection.connstring);
            SqlCommand cmd = new SqlCommand("sp_Update_Wallet", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Net_Amount", walletBal);
            cmd.Parameters.AddWithValue("@Id", WalletId);
            cmd.Parameters.AddWithValue("@Modify_Date", Connection.indianTime);
            try
            {

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                status = 200;

            }
            catch (Exception ex)
            {
                con.Close();
                cmd.Dispose();
                status = 400;

            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return status;
        }

        public int Update_Oder_tbl(int OrderId)
        {
            int status = 0;
            SqlConnection con = new SqlConnection(Connection.connstring);
            SqlCommand cmd = new SqlCommand("sp_Update_Order_tbl", con);
            cmd.CommandType = CommandType.StoredProcedure;            
            cmd.Parameters.AddWithValue("@Id", OrderId);
            cmd.Parameters.AddWithValue("@PaymentTypeId", paymenttype);
            cmd.Parameters.AddWithValue("@OrderStatusId", 3);//Picked
           // cmd.Parameters.AddWithValue("@PaymentStatusId", PaymentStatusId);
            try
            {

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                status = 200;

            }
            catch (Exception ex)
            {
                con.Close();
                cmd.Dispose();
                status = 400;

            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return status;
        }

        // PUT: api/OrderDetail_tbl/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrderDetail_tbl(int id, OrderDetail_tbl orderDetail_tbl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != orderDetail_tbl.Id)
            {
                return BadRequest();
            }

            db.Entry(orderDetail_tbl).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderDetail_tblExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/OrderDetail_tbl
        [ResponseType(typeof(OrderDetail_tbl))]
        public async Task<IHttpActionResult> PostOrderDetail_tbl(OrderDetail_tbl orderDetail_tbl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            orderDetail_tbl.IsActive = true;
            orderDetail_tbl.IsDeleted = false;
            orderDetail_tbl.CreateDate =  Connection.indianTime;           
            db.OrderDetail_tbls.Add(orderDetail_tbl);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = orderDetail_tbl.Id }, orderDetail_tbl);
        }

        // DELETE: api/OrderDetail_tbl/5
        [ResponseType(typeof(OrderDetail_tbl))]
        public async Task<IHttpActionResult> DeleteOrderDetail_tbl(int id)
        {
            OrderDetail_tbl orderDetail_tbl = await db.OrderDetail_tbls.FindAsync(id);
            if (orderDetail_tbl == null)
            {
                return NotFound();
            }

            db.OrderDetail_tbls.Remove(orderDetail_tbl);
            await db.SaveChangesAsync();

            return Ok(orderDetail_tbl);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderDetail_tblExists(int id)
        {
            return db.OrderDetail_tbls.Count(e => e.Id == id) > 0;
        }
    }
}