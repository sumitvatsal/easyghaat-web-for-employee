﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;

namespace EmployeeEasyGhaat.Controllers
{
    public class RoleTypesController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // GET: api/RoleTypes
        public IQueryable<RoleType> GetRoleTypes()
        {
            return db.RoleTypes;
        }

        // GET: api/RoleTypes/id
        [ResponseType(typeof(RoleType))]
        public async Task<IHttpActionResult> GetRoleType(int id)
        {
            RoleType roleType = await db.RoleTypes.FindAsync(id);
            if (roleType == null)
            {
                return NotFound();
            }

            return Ok(roleType);
        }

        // PUT: api/RoleTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRoleType(int id, RoleType roleType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != roleType.Id)
            {
                return BadRequest();
            }

            db.Entry(roleType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RoleTypes
        [ResponseType(typeof(RoleType))]
        public async Task<IHttpActionResult> PostRoleType(RoleType roleType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RoleTypes.Add(roleType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = roleType.Id }, roleType);
        }

        // DELETE: api/RoleTypes/5
        [ResponseType(typeof(RoleType))]
        public async Task<IHttpActionResult> DeleteRoleType(int id)
        {
            RoleType roleType = await db.RoleTypes.FindAsync(id);
            if (roleType == null)
            {
                return NotFound();
            }

            db.RoleTypes.Remove(roleType);
            await db.SaveChangesAsync();

            return Ok(roleType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoleTypeExists(int id)
        {
            return db.RoleTypes.Count(e => e.Id == id) > 0;
        }
    }
}