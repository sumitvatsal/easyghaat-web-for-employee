﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Data.SqlClient;
using System.Collections;


namespace EmployeeEasyGhaat.Controllers
{
    public class DeliveryController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        [Route("api/Delivery/ByTaskId/{TaskId}")]
        [HttpGet]
        public async Task<int> Delivery(int TaskId)
        {
            //Response Received through status Code:
            //200 => OK(Update Successfully)
            //300 => Order Not Exist
            //400 => Error(Error Occurred)
            //404 => Not Found(Entry No exist)


            int status = 0;
            try
            {
                var Task_assgn_dtls = db.TaskAssignment_tbls.SingleOrDefault(tsk => tsk.Id == TaskId);
                if (Task_assgn_dtls != null)
                {
                    var Order_dtls = db.Order_tbls.SingleOrDefault(od => od.Id == Task_assgn_dtls.OrderId);
                    if (Order_dtls != null)
                    {
                        //Checking Payment Status
                        if (Order_dtls.PaymentStatusId == 1)//for paid
                        {
                            // Call Delivery Table
                            status = Add_OrderDelivery(TaskId, Order_dtls.Id);
                        }
                        else//Unpaid
                        {
                            if (Order_dtls.PaymentTypeId == 1) //COD=1,
                            {
                                var Cus_Inv = db.CustomerInvoice_Tbls.SingleOrDefault(cus => cus.OrderId == Order_dtls.Id);
                                if (Cus_Inv.NetBalance > 0)
                                {
                                    status = Add_EmpCashReceiving(Task_assgn_dtls.Id, Cus_Inv);
                                }
                                else
                                {
                                    // Call Delivery Table
                                    status = Add_OrderDelivery(TaskId, Order_dtls.Id);
                                }

                            }
                            else if (Order_dtls.PaymentTypeId == 2)//COP=2
                            {
                                // Call Delivery Table
                                status = Add_OrderDelivery(TaskId, Order_dtls.Id);
                            }
                        }
                    }
                    else
                    {
                        status = 300;
                    }
                }
                else
                {
                    status = 404;
                }
            }
            catch(Exception ex)
            {
                status = 400;
            }
            return status;


        }
       
        public int Add_EmpCashReceiving(int TaskId, CustomerInvoice_Tbl Cus_Inv)
        {
            int status = 0;
            try
            {

                EmpCashReceiving_Tbl emp = new EmpCashReceiving_Tbl();
                emp.TaskId = TaskId;
                emp.TotalAmount = Cus_Inv.NetBalance;
                emp.IsHandOver = false;
                emp.IsActive = true;
                emp.IsDeleted = false;
                emp.CreateDate = Connection.indianTime;

                if (!ModelState.IsValid)
                {
                    status = 400;
                    return status;
                }
                db.EmpCashReceiving_Tbls.Add(emp);
                db.SaveChanges();

                //Updating Payment Status as Paid
                var Ord_dtls = db.Order_tbls.SingleOrDefault(o => o.Id == Cus_Inv.OrderId);
                if (Ord_dtls != null)
                {
                    Ord_dtls.PaymentStatusId = 1;
                    db.SaveChanges();
                }

                //Here checking CashBackAmount
                if (Cus_Inv.CashBackAmount > 0)
                {
                    status = Add_Cashbck(TaskId, Cus_Inv);
                }
                else
                {
                    // Call Delivery Table
                    status = Add_OrderDelivery(TaskId, Cus_Inv.OrderId);
                }

                status = 200;
            }
            catch(Exception ex)
            {
                status = 400;
            }
            return status;
        }
        public int Add_Cashbck(int TaskId,CustomerInvoice_Tbl Cus_Inv)
        {
            int status = 0, CustomerId=0;
            try
            {
                var Order_dts = db.Order_tbls.SingleOrDefault(od => od.Id == Cus_Inv.OrderId);
                CustomerId = Convert.ToInt32(Order_dts.CustomerId);
                Cashback_tbl csh_bk = new Cashback_tbl();

                csh_bk.OrderId = Cus_Inv.OrderId;
                csh_bk.Cashback_type = "Scehme";
                csh_bk.CustomerId = CustomerId;
                csh_bk.Amount = Cus_Inv.CashBackAmount;
                csh_bk.IsActive = true;
                csh_bk.IsDeleted = false;
                csh_bk.CreateDate = Connection.indianTime;
                csh_bk.Trans_Date = Connection.indianTime;
                if (!ModelState.IsValid)
                {
                    status = 400;
                    return status;
                }

                db.Cashback_tbls.Add(csh_bk);
                db.SaveChanges();
                status = 200;
                if (status == 200)
                {
                    status = Add_Cashbck_trans(Cus_Inv, csh_bk.Id, CustomerId, TaskId);
                }
            }
            catch(Exception ex)
            {
                status = 400;
            }

            return status;
        }
        public int Add_Cashbck_trans(CustomerInvoice_Tbl Cus_Inv, int CshId,int CustomerId,int TaskId)
        {
            int status = 0;
            try
            {
                var wall_dtls = db.Wallet_tbls.SingleOrDefault(w => w.CustomerId == CustomerId);
                if (wall_dtls != null)
                {
                    Transaction_tbl transaction_tbl = new Models.Transaction_tbl();
                    double walletBal = 0;
                    walletBal = wall_dtls.Net_Amount + Cus_Inv.CashBackAmount;
                    transaction_tbl.WalletId = wall_dtls.Id;
                    transaction_tbl.Trans_Type = "Cr.";
                    transaction_tbl.Amount = Cus_Inv.CashBackAmount;
                    transaction_tbl.Closing_Bal = walletBal;
                    transaction_tbl.CashbckId = CshId;
                    transaction_tbl.OrderId = Cus_Inv.OrderId;
                    transaction_tbl.Trans_Date = Connection.indianTime;
                    transaction_tbl.CreateDate = Connection.indianTime;
                    transaction_tbl.IsActive = true;
                    transaction_tbl.IsDeleted = false;
                    if (!ModelState.IsValid)
                    {
                        status = 400;
                        return status;
                    }

                    db.Transaction_tbls.Add(transaction_tbl);
                    db.SaveChanges();
                    status = 200;
                    if (status == 200)
                    {
                        status = Update_wallet(walletBal, wall_dtls.Id);
                        if (status == 200)
                        {
                            // Call Delivery Table
                            status = Add_OrderDelivery(TaskId, Cus_Inv.OrderId);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                status = 400;
            }
         
            return status;
        }

        //Insert value in Delivery Table
        public int Add_OrderDelivery(int TaskId, int OrderId)
        {
            int status = 0;
            try
            {
                OrderDelivery_tbl ord_dly = new OrderDelivery_tbl();
                ord_dly.TaskId = TaskId;
                ord_dly.DeliveryDate = Connection.indianTime;
                ord_dly.IsActive = true;
                ord_dly.IsDeleted = false;
                ord_dly.CreateDate = Connection.indianTime;
                if (!ModelState.IsValid)
                {
                    status = 400;
                    return status;
                }

                db.OrderDelivery_tbls.Add(ord_dly);
                db.SaveChanges();
                status = 200;
                if (status == 200)
                {
                    status = Update_Oder_tbl(OrderId);

                }
            }
            catch(Exception ex)
            {
                status = 400;
            }
            return status;
        }

        //here Updating Order Status as Delivered 
        public int Update_Oder_tbl(int OrderId)
        {
            int status = 0;
            try
            {

                var Ord_dtls = db.Order_tbls.SingleOrDefault(o => o.Id == OrderId);
                if (Ord_dtls != null)
                {
                    Ord_dtls.OrderStatusId = 6;//OrderStatusId set Delivered              
                    db.SaveChanges();
                    status = 200;
                }
                else
                {
                    status = 400;
                }
            }
            catch(Exception ex)
            {
                status = 400;
            }
            return status;
        }

        public int Update_wallet(double walletBal, int WalletId)
        {
            int status = 0;
            SqlConnection con = new SqlConnection(Connection.connstring);
            SqlCommand cmd = new SqlCommand("sp_Update_Wallet", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Net_Amount", walletBal);
            cmd.Parameters.AddWithValue("@Id", WalletId);
            cmd.Parameters.AddWithValue("@Modify_Date", Connection.indianTime);
            try
            {

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                cmd.Dispose();
                status = 200;

            }
            catch (Exception ex)
            {
                con.Close();
                cmd.Dispose();
                status = 400;

            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return status;
        }


    }
}
