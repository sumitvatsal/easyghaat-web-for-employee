﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Data.SqlClient;

namespace EasyGhaat.Areas.Vendordashboard.Controllers
{
    public class EmployeeNotificationsController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        [HttpGet] 
        [ActionName("EmpNotification")]
        public async Task<IHttpActionResult> EmpNotification(int EmpId)
        {          
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(Connection.connstring);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_EmployeeNotification", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpId", EmpId);
            using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
            {
                dt.Load(reader);
            }
            if ((dt == null) || (dt.Rows.Count == 0))
            {
                con.Close();
                return NotFound();
            }
            con.Close();
            return Ok(dt);
        }
















    }
}
