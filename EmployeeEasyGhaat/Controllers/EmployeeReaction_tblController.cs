﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Globalization;

namespace EasyGhaat.Areas.Vendordashboard.Controllers
{
    public class EmployeeReaction_tblController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();
        // GET: api/EmployeeReaction_tbl
        public IQueryable<EmployeeReaction_tbl> GetEmployeeReaction_tbls()
        {
            return db.EmployeeReaction_tbls;
        }

        // GET: api/EmployeeReaction_tbl/5
        [ResponseType(typeof(EmployeeReaction_tbl))]
        public async Task<IHttpActionResult> GetEmployeeReaction_tbl(int id)
        {
            EmployeeReaction_tbl employeeReaction_tbl = await db.EmployeeReaction_tbls.FindAsync(id);
            if (employeeReaction_tbl == null)
            {
                return NotFound();
            }

            return Ok(employeeReaction_tbl);
        }

        // PUT: api/EmployeeReaction_tbl/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutEmployeeReaction_tbl(int id, EmployeeReaction_tbl employeeReaction_tbl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employeeReaction_tbl.Id)
            {
                return BadRequest();
            }

            db.Entry(employeeReaction_tbl).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeReaction_tblExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/EmployeeReaction_tbl/PostEmployeeReaction_tblAccpeted
        [ResponseType(typeof(EmployeeReaction_tbl))]
        public async Task<IHttpActionResult> PostEmployeeReaction_tblAccpeted(int TaskId, int EmployeeId)
        {
            EmployeeReaction_tbl employeeReaction_tbl = new EmployeeReaction_tbl();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            employeeReaction_tbl.TaskId = TaskId;
            employeeReaction_tbl.EmpAccepted = true;
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            employeeReaction_tbl.ReactionDateTime = indianTime;
            employeeReaction_tbl.EmployeeId = EmployeeId;
            db.EmployeeReaction_tbls.Add(employeeReaction_tbl);
            await db.SaveChangesAsync();
            return Ok("Successfull Accpeted");
        }


        // POST: api/EmployeeReaction_tbl/PostEmployeeReaction_tblRejected
        [ResponseType(typeof(EmployeeReaction_tbl))]
        public async Task<IHttpActionResult> PostEmployeeReaction_tblRejected(int TaskId, int RejectionReasonId,int EmployeeId)
        {
            EmployeeReaction_tbl employeeReaction_tbl = new EmployeeReaction_tbl();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            employeeReaction_tbl.TaskId = TaskId;
            employeeReaction_tbl.EmpAccepted = false;
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            employeeReaction_tbl.ReactionDateTime = indianTime;
            employeeReaction_tbl.RejectionReasonId = RejectionReasonId;
            employeeReaction_tbl.EmployeeId = EmployeeId;
            employeeReaction_tbl.IsReassigned = false;
            db.EmployeeReaction_tbls.Add(employeeReaction_tbl);
            await db.SaveChangesAsync();
            return Ok("Successfully Rejectd");
        }

        // DELETE: api/EmployeeReaction_tbl/5
        [ResponseType(typeof(EmployeeReaction_tbl))]
        public async Task<IHttpActionResult> DeleteEmployeeReaction_tbl(int id)
        {
            EmployeeReaction_tbl employeeReaction_tbl = await db.EmployeeReaction_tbls.FindAsync(id);
            if (employeeReaction_tbl == null)
            {
                return NotFound();
            }

            db.EmployeeReaction_tbls.Remove(employeeReaction_tbl);
            await db.SaveChangesAsync();
            return Ok(employeeReaction_tbl);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeReaction_tblExists(int id)
        {
            return db.EmployeeReaction_tbls.Count(e => e.Id == id) > 0;
        }
    }
}