﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Globalization; // For Adding TimeZone


namespace EmployeeEasyGhaat.Controllers
{
    public class EmployeeSessionsController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();
        // public DateTime dt = DateTime.Now;


        // GET: api/EmployeeSessions
        public IQueryable<EmployeeSession> GetEmployeeSessions()
        {
            return db.EmployeeSessions;
        }

        // GET: api/EmployeeSessions/5
        [ResponseType(typeof(EmployeeSession))]
        public async Task<IHttpActionResult> GetEmployeeSession(int id)
        {
            EmployeeSession employeeSession = await db.EmployeeSessions.FindAsync(id);
            if (employeeSession == null)
            {
                return NotFound();
            }

            return Ok(employeeSession);
        }

        //PUT:api/EmployeeSessions/PutEmployeeSessionLogOut/EmployeeId         
        [ResponseType(typeof(EmployeeSession))]
        public async Task<IHttpActionResult> PutEmployeeSessionLogOut(int SessionId)
        {
            EmployeeSession employeeSession = new EmployeeSession();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (SessionId == 0)
            {
                return BadRequest();
            }
            employeeSession = db.EmployeeSessions.Where(a => a.Id == SessionId).SingleOrDefault();
            try
            {
            if (employeeSession != null)
            {
                employeeSession.Id = SessionId;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                employeeSession.LogOut_DateTime = indianTime;
                db.Entry(employeeSession).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Ok("Successfully LogOut");
            }                    
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeSessionExists(SessionId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EmployeeSessions/PostEmployeeSessionLogin/EmployeeId/LoginFrom
        [ResponseType(typeof(EmployeeSession))]
        public async Task<IHttpActionResult> PostEmployeeSessionLogin( int EmployeeId, string LoginFrom)
        {
            EmployeeSession employeeSession = new EmployeeSession();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            employeeSession.EmployeeId = EmployeeId;
            employeeSession.LoginFrom = LoginFrom;
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            employeeSession.Login_DateTime = indianTime;
            db.EmployeeSessions.Add(employeeSession);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = employeeSession.Id }, employeeSession);
           
        }

        // DELETE: api/EmployeeSessions/5
        [ResponseType(typeof(EmployeeSession))]
        public async Task<IHttpActionResult> DeleteEmployeeSession(int id)
        {
            EmployeeSession employeeSession = await db.EmployeeSessions.FindAsync(id);
            if (employeeSession == null)
            {
                return NotFound();
            }
            db.EmployeeSessions.Remove(employeeSession);
            await db.SaveChangesAsync();
            return Ok(employeeSession);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeSessionExists(int id)
        {
            return db.EmployeeSessions.Count(e => e.Id == id) > 0;
        }
    }
}