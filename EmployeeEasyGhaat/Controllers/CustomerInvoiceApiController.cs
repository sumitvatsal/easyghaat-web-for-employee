﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestProjects.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.IO;
using System.Text;

namespace TestProjects.Controllers
{
    public class CustomerInvoiceApiController : ApiController
    {
        string body = string.Empty;
        private EasyGhaatDbContext db = new EasyGhaatDbContext();

        // GET api/CustomerInvoiceApi/12
        public int Get(int id)
        {
            //Response Received through status Code:
            //200 => OK(Email Sent Successfully with Paynow Button)
            //300 => Already paid(Email Sent Successfully without Paynow Button)
            //400 => Error(Error Occurred)
            //404 => EmailId Not Found

            int status = 0;

            try
            {
                CustomerInvoiceEmailPageData CustomerInvoice_Obj = new CustomerInvoiceEmailPageData();
                CustomerInvoice_Obj.Order_tbl = db.Order_tbl.Where(o => o.Id == id).SingleOrDefault();
                CustomerInvoice_Obj.OrderDetail_tbl = db.OrderDetail_tbl.Where(o => o.OrderId == CustomerInvoice_Obj.Order_tbl.Id).ToList();
                CustomerInvoice_Obj.DeliveryType_tbl = db.DeliveryType_tbl.Where(o => o.Id == CustomerInvoice_Obj.Order_tbl.DeliveryTypeId).SingleOrDefault();
                CustomerInvoice_Obj.CustomerInvoice_Tbl = db.CustomerInvoice_Tbl.Where(o => o.OrderId == id).SingleOrDefault();
                //From Down
                CustomerInvoice_Obj.PickUpAddress = db.Address.Where(a => a.Id == CustomerInvoice_Obj.Order_tbl.PickUpAddressId).SingleOrDefault();
                CustomerInvoice_Obj.DeliveryAddress = db.Address.Where(a => a.Id == CustomerInvoice_Obj.Order_tbl.DeliveryAddressId).SingleOrDefault();
                CustomerInvoice_Obj.PickUpTimeSlot = db.TimeSlot.Where(t => t.id == CustomerInvoice_Obj.Order_tbl.PickUpTimeSlotId).SingleOrDefault();
                CustomerInvoice_Obj.DeliveryTimeSlot = db.TimeSlot.Where(t => t.id == CustomerInvoice_Obj.Order_tbl.DeliveryTimeSlotId).SingleOrDefault();
                CustomerInvoice_Obj.Customer_Register = db.Customer_Register.Where(c => c.Id == CustomerInvoice_Obj.Order_tbl.CustomerId).SingleOrDefault();
                CustomerInvoice_Obj.DeliveryCity = db.City.Where(c => c.Id == CustomerInvoice_Obj.DeliveryAddress.CityId).SingleOrDefault();
                CustomerInvoice_Obj.PickUpCity = db.City.Where(c => c.Id == CustomerInvoice_Obj.PickUpAddress.CityId).SingleOrDefault();
                //-----------------
                List<PriceTable> price_list = new List<PriceTable>();
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["EasyghaatDBCon"].ConnectionString);
                SqlCommand cmd;
                System.Data.DataSet ds;
                SqlDataAdapter adp;
                //foreach (OrderDetail_tbl order in CustomerInvoice_Obj.OrderDetail_tbl)
                //{
                //    PriceTable p = db.PriceTable.Where(p1 => p1.Id == order.PriceId).SingleOrDefault();
                //    price_list.Add(p);
                //}
                CustomerInvoice_Obj.PriceTable = price_list;
                //-----------------------

                //-----------------
                List<MyClothesClass> myclothes = new List<MyClothesClass>();
                cmd = new SqlCommand("GetClothsDataFromTables", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                adp = new SqlDataAdapter(cmd);
                ds = new System.Data.DataSet();
                adp.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        MyClothesClass c = new MyClothesClass();
                        c.name = Convert.ToString(ds.Tables[0].Rows[i][0]);
                        c.quantity = Convert.ToInt32(ds.Tables[0].Rows[i][1]);
                        myclothes.Add(c);
                    }
                }
                CustomerInvoice_Obj.MyClothesClass = myclothes;
                var root = AppDomain.CurrentDomain.BaseDirectory;
                using (StreamReader reader = new StreamReader(root + @"/Content/email.html"))
                {
                    body = reader.ReadToEnd();

                }
                body = body.Replace("{name}", CustomerInvoice_Obj.Customer_Register.Name);
                body = body.Replace("{Invoice_Date}", Convert.ToString((CustomerInvoice_Obj.CustomerInvoice_Tbl.InvoiceDateTime).ToShortDateString()));
                body = body.Replace("{final_amount}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.NetBalance));
                body = body.Replace("{delivery_type}", CustomerInvoice_Obj.DeliveryType_tbl.DeliveryType);
                //Pick Up Details
                body = body.Replace("{Pickup_date}", (Convert.ToString(CustomerInvoice_Obj.Order_tbl.PickUpDate).Substring(0, 10)));
                body = body.Replace("{Pickup_Timeslot}", Convert.ToString(CustomerInvoice_Obj.PickUpTimeSlot.FromTime) + " - " + Convert.ToString(CustomerInvoice_Obj.PickUpTimeSlot.ToTime));
                body = body.Replace("{Pickup_Address}", CustomerInvoice_Obj.PickUpAddress.HosueNo + "," + CustomerInvoice_Obj.PickUpAddress.StreetName + "," + CustomerInvoice_Obj.PickUpAddress.AreaId + "," + CustomerInvoice_Obj.PickUpAddress.LandMark + "," + CustomerInvoice_Obj.PickUpCity.CityName);
                //Delivery Details
                body = body.Replace("{Delivery_Date}", (Convert.ToString(CustomerInvoice_Obj.Order_tbl.DeliveryDate)).Substring(0, 10));
                body = body.Replace("{Delivery_TimeSlot}", Convert.ToString(CustomerInvoice_Obj.DeliveryTimeSlot.FromTime) + " - " + Convert.ToString(CustomerInvoice_Obj.DeliveryTimeSlot.ToTime));
                body = body.Replace("{Delivery_Address}", CustomerInvoice_Obj.DeliveryAddress.HosueNo + "," + CustomerInvoice_Obj.DeliveryAddress.StreetName + "," + CustomerInvoice_Obj.DeliveryAddress.AreaId + "," + CustomerInvoice_Obj.DeliveryAddress.LandMark + "," + CustomerInvoice_Obj.DeliveryCity.CityName);
                //Invoice Break Up
                body = body.Replace("{Basic_Charges}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.SubAmount));
                body = body.Replace("{P_D_Charges}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.PickUp_DeliveryCharges));
                body = body.Replace("{Surging_Charge}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.SurgingAmount));
                body = body.Replace("{LaundryBag_Charges}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.LaundryBagAmount));
                body = body.Replace("{Package_Adjustment}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.PackageAdjustmentAmt));
                body = body.Replace("{SubTotal}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.SubAmount + CustomerInvoice_Obj.CustomerInvoice_Tbl.PickUp_DeliveryCharges + CustomerInvoice_Obj.CustomerInvoice_Tbl.SurgingAmount + CustomerInvoice_Obj.CustomerInvoice_Tbl.LaundryBagAmount - CustomerInvoice_Obj.CustomerInvoice_Tbl.PackageAdjustmentAmt));
                body = body.Replace("{ServiceTax}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.ServiceTaxAmt + CustomerInvoice_Obj.CustomerInvoice_Tbl.KrishiKalyanCessAmt + CustomerInvoice_Obj.CustomerInvoice_Tbl.SwachhBharatCess));
                body = body.Replace("{Net_Amount}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.TotalAmount));
                body = body.Replace("{Wallet_Adjustment}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.WalletAdjustment));
                body = body.Replace("{Cashback}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.CashBackAmount));
                body = body.Replace("{Cashback}", Convert.ToString(CustomerInvoice_Obj.CustomerInvoice_Tbl.CashBackAmount));
                body = body.Replace("{Share_Code}", CustomerInvoice_Obj.Customer_Register.MyReferalCode);

                // Cloth Description & Distinct Services
                var tr = "<tr style =" + "border-bottom: 1px solid black;" + ">";
                var td = "<td style =" + "font-family: sans-serif; font-size: 12px; mso-height-rule: exactly; line-height: 27px;font-weight: 400; color: #555555;" + ">";
                var ClothDesc = "";
                var TotalClothCount = 0;
                var DistinctService = "";
                foreach (var item in CustomerInvoice_Obj.MyClothesClass)
                {
                    ClothDesc = ClothDesc + tr + td + item.name + "</td>" + td + item.quantity + "</td></tr>";

                    TotalClothCount += Convert.ToInt32(item.quantity);
                    DistinctService += item.name + ",";

                }

                body = body.Replace("{ClothesDescription}", ClothDesc);
                body = body.Replace("{TotalClothCount}", TotalClothCount.ToString());
                body = body.Replace("{services}", DistinctService);

                // var pay_btn = "<td align='right' style='border-radius:3px;background:#32c2e5;float:right;width:177px;margin-top:14px;margin-bottom:16px;' class='button-td'><a style='display:none;background:#32c2e5;border:15px solid #32c2e5;color:#ffffff;font-family:sans-serif;font-size:10px;line-height:1.1;text-align:center;text-decoration:none;display:block;border-radius:3px;font-weight:bold;' class='button-a' href =http://easyghaat.cashbachat.com/OrderPayment/Index?OrderId=" + id+">"+
                var pay_btn = "<td align='right' style='border-radius:3px;background:#32c2e5;float:right;width:177px;margin-top:14px;margin-bottom:16px;' class='button-td'><a style='display:none;background:#32c2e5;border:15px solid #32c2e5;color:#ffffff;font-family:sans-serif;font-size:10px;line-height:1.1;text-align:center;text-decoration:none;display:block;border-radius:3px;font-weight:bold;' class='button-a' href =http://localhost:1044/OrderPayment/Index?OrderId=" + id + ">" +
                "<strong> Pay <img src='http://www.easyghaat.com/NewImages/ru2.png' style='margin-bottom:-12px;'/><span style='font-size:18px;'>" + CustomerInvoice_Obj.CustomerInvoice_Tbl.NetBalance + " Now </span></strong></a></td>";

                if (CustomerInvoice_Obj.Order_tbl.PaymentStatusId == 2 && CustomerInvoice_Obj.CustomerInvoice_Tbl.NetBalance > 0)
                {
                    body = body.Replace("{pay}", pay_btn);
                    if (CustomerInvoice_Obj.Customer_Register.EmailId.Length != 0)
                    {
                        SendEmailAsync(CustomerInvoice_Obj.Customer_Register.EmailId, body);
                        status = 200;
                    }
                    else
                    {
                        status = 404;
                    }

                }
                else
                {
                    body = body.Replace("{pay}", " ");
                    if (CustomerInvoice_Obj.Customer_Register.EmailId.Length != 0)
                    {
                        SendEmailAsync(CustomerInvoice_Obj.Customer_Register.EmailId, body);
                    }
                    status = 300;
                }
            }
            catch(Exception ex)
            {
                status = 400;
            }
            return status;

        }

        

        [System.Web.Http.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendEmailAsync(string email, string body)
        {
            var _email = "shubhamjain@easyghaat.com";
            var _epass = "Easyghaat@1";
            var _disName = "EasyGhaat";
            MailMessage mymessage = new MailMessage();
            mymessage.To.Add(email);
            mymessage.From = new MailAddress(_email, _disName);
            mymessage.Subject = "Easyghaat Invoice";
            mymessage.Body = body;
            mymessage.IsBodyHtml = true;

            using (SmtpClient smtp = new SmtpClient())
            {
                //smtp.EnableSsl = true;
                smtp.Host = "smtpout.europe.secureserver.net";
                smtp.Port = 3535;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_email, _epass);
                smtp.Timeout = 5000000;
                //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtp.SendCompleted += (s, e) => { smtp.Dispose(); };
                smtp.Send(mymessage);
            }

            return null;
        }

    }
}
