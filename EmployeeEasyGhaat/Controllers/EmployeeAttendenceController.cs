﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeEasyGhaat.Models;
using System.Data.SqlClient;
using System.Collections;

namespace EmployeeEasyGhaat.Controllers
{
    public class EmployeeAttendenceController : ApiController
    {
        private EmployeeContext db = new EmployeeContext();

        // POST: api/EmployeeAttendence
        [HttpPost]
        public async Task<IHttpActionResult> AddEmployeeAttendence(EmployeeAttendence Emp_att)
        {
            //Response Received through status Code:
            //200 => OK(Add Successfully)
            //300 => OK(Already Entry)
            //400 => Error(Error Occurred)

            var date = Connection.indianTime;
            var cur_date = date.Date;

            try
            {
                var Emp_Att_dtls = db.EmployeeAttendences.Where(e => e.EmpId == Emp_att.EmpId && e.Date == cur_date);
                if (Emp_Att_dtls.Count() > 0)
                {
                    return Ok(300);
                }
                else
                {
                    Emp_att.Date = Connection.indianTime;
                    Emp_att.InTime = Connection.indianTime;
                    Emp_att.IsActive = true;
                    Emp_att.IsDeleted = false;
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }
                    db.EmployeeAttendences.Add(Emp_att);
                    await db.SaveChangesAsync();
                    return Ok(200);
                }
            }
            catch(Exception ex)
            {
                return Ok(400);
            }

          
        }


        [Route("api/EmployeeAttendence/Outtime/ByEmpId/{EmpId}")]
        [HttpGet]
        public async Task<int> OuttimeEmployeeAttendence(Int64 EmpId)
        {
            //Response Received through status Code:
            //200 => OK(Update Successfully)
            //300 => NO(Entry Already Updated)
            //400 => Error(Error Occurred)
            //404 => Not Found(Entry No exist)


            int status = 0;
            try
            {
                var date = Connection.indianTime;
                var c_date = date.Date;
                var Emp_Att_dtls = db.EmployeeAttendences.SingleOrDefault(e => e.EmpId == EmpId && e.Date == c_date);
                if (Emp_Att_dtls != null)
                {
                    if (Emp_Att_dtls.OutTime == null)
                    {
                        var cur_date = Connection.indianTime;
                        Emp_Att_dtls.OutTime = cur_date;
                        //Get the difference of two date in hours                  
                        var hours = (cur_date - Emp_Att_dtls.InTime).TotalHours;
                        if(hours>0)
                        {
                            Emp_Att_dtls.TotalActiveHours = hours;
                        }
                        else
                        {
                            Emp_Att_dtls.TotalActiveHours = 0;
                        }                      
                        
                        await db.SaveChangesAsync();
                        status = 200;
                    }
                    else
                    {
                        status = 300;
                    }

                }
                else
                {
                    status = 404;
                }
            }
            catch(Exception ex)
            {
                status = 400;
            }
            return status;
           

        }
    }
}
